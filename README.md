# flash-webview

[![CI Status](http://img.shields.io/travis/git/flash-webview.svg?style=flat)](https://travis-ci.org/git/flash-webview)
[![Version](https://img.shields.io/cocoapods/v/flash-webview.svg?style=flat)](http://cocoapods.org/pods/flash-webview)
[![License](https://img.shields.io/cocoapods/l/flash-webview.svg?style=flat)](http://cocoapods.org/pods/flash-webview)
[![Platform](https://img.shields.io/cocoapods/p/flash-webview.svg?style=flat)](http://cocoapods.org/pods/flash-webview)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

flash-webview is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'flash-webview'
```

## Author

git, alexs@2kgroup.com

## License

flash-webview is available under the MIT license. See the LICENSE file for more info.
