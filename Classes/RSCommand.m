//
//  RSCommand.m
//  flash-webview
//
//  Created by Alexander Semenov on 3/16/18.
//

#import "RSCommand.h"

NSString * const keyId = @"id";
NSString * const keyObjectId = @"objectId";
NSString * const keyCommand = @"command";
NSString * const keyFunction = @"function";
NSString * const keyParam = @"param";
NSString * const keyNeedResult = @"needResult";

NSString *const OBJECT_ID_RECOGNITION = @"recognition_object";
NSString *const OBJECT_ID_RECORD_AND_PLAY = @"record_and_play_object";
NSString *const kCommandInvoke = @"invoke";
NSString *const kCommandLoad = @"load";
NSString *const kCommandResult = @"result";
NSString *const kCommandAudio = @"audio";
NSString *const kCommandClose = @"close";


@implementation RSCommand

-(NSData *) serialize {
    NSMutableDictionary * json = [NSMutableDictionary dictionary];
    if (self.clientId) { json[keyId] = self.clientId; }
    if (self.objectId) { json[keyObjectId] = self.objectId; }
    if (self.command) { json[keyCommand] = self.command; }
    if (self.function) { json[keyFunction] = self.function; }
    if (self.param) { json[keyParam] = self.param; }
    json[keyNeedResult] = @(self.needResult);

    NSError * error;
    NSData * result = [NSJSONSerialization dataWithJSONObject:json options:0 error: &error];
    if (error) {
        NSLog(@"Failed to serialize command, %@", error);
    }
    return result;

}

+(RSCommand *) parse: (NSData *) input {
    NSError * error;
    NSDictionary * json = [NSJSONSerialization JSONObjectWithData:input options:NSJSONReadingAllowFragments error:&error];
    if (json == nil) {
        NSLog(@"SOCKET: Did fail to parse command: %@", error);
        return nil;
    }

    RSCommand * result = [[RSCommand alloc] init];
    result.objectId = json[keyObjectId];
    result.command = json[keyCommand];
    result.function = json[keyFunction];
    result.param = json[keyParam];
    result.needResult = [json[keyNeedResult] boolValue];

    return result;
}

- (NSString *)description {
    NSMutableString *description = [NSMutableString stringWithFormat:@"<%@: ", NSStringFromClass([self class])];
    [description appendFormat:@"self.clientId=%@", self.clientId];
    [description appendFormat:@", self.objectId=%@", self.objectId];
    [description appendFormat:@", self.command=%@", self.command];
    [description appendFormat:@", self.function=%@", self.function];
    [description appendFormat:@", self.param=%@", self.param];
    [description appendFormat:@", self.needResult=%d", self.needResult];
    [description appendString:@">"];
    return description;
}

@end
