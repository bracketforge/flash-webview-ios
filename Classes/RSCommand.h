//
//  RSCommand.h
//  flash-webview
//
//  Created by Alexander Semenov on 3/16/18.
//

#import <Foundation/Foundation.h>

extern NSString *const OBJECT_ID_RECOGNITION;
extern NSString *const OBJECT_ID_RECORD_AND_PLAY;
extern NSString *const kCommandInvoke;
extern NSString *const kCommandLoad;
extern NSString *const kCommandResult;
extern NSString *const kCommandAudio;
extern NSString *const kCommandClose;

@interface RSCommand : NSObject

@property(nonatomic) NSNumber * clientId;
@property(nonatomic) NSString * objectId;
@property(nonatomic) NSString * command;
@property(nonatomic) NSString * function;
@property(nonatomic) NSArray * param;
@property(nonatomic) BOOL needResult;

-(NSData *) serialize;

- (NSString *)description;
+(RSCommand *) parse: (NSData *) input;

@end
