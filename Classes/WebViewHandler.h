//
//  WebViewHandler.h
//  FlashWebView
//
//  Created by Alexander Semenov on 3/12/18.
//  Copyright © 2018 Alexander Semenov. All rights reserved.
//

#import <Foundation/Foundation.h>
@import WebKit;

@interface WebViewHandler : NSObject

@property(strong, nonatomic, readonly) UIWebView * webView;
@property(readonly, nonatomic) NSDictionary * replaceMap;

+(WebViewHandler *) sharedInstance;

-(void) setupWebView: (UIWebView *) webView;

-(void) setupWebViewContainer: (WKWebView *) view url: (NSString *) url;

-(void) restart;

-(NSString *) interceptHttpRequest: (NSURLRequest *) request;

@end

