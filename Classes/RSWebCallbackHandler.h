//
//  RSWebCallbackHandler.h
//  flash-webview
//
//  Created by Alexander Semenov on 3/15/18.
//

#import <Foundation/Foundation.h>

@interface RSWebCallbackHandler : NSObject

@property (copy, nonatomic) NSString * (^jsExecutor)(NSString * script);

@property (readonly) BOOL isRecognitionPage;

-(void) openConnection;

- (void)closeConnection;

-(BOOL)handleUrl: (NSURL *) url;

@end
