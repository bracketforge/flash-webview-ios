//
//  AudioProcessor.m
//  MicInput
//
//  Created by Stefan Popp on 21.09.11.
//  Copyright 2011 http://www.stefanpopp.de/2011/capture-iphone-microphone/ . All rights reserved.
//

#import "AudioProcessor.h"
@import AVFoundation;

// return max value for given values
#define max(a, b) (((a) > (b)) ? (a) : (b))
// return min value for given values
#define min(a, b) (((a) < (b)) ? (a) : (b))

#define kOutputBus 0
#define kInputBus 1

// our default sample rate
#define SAMPLE_RATE 8000.00
// maximum buffer size in bytes, which will be sent to callback
#define MAX_BUFFER_SIZE 2048

#pragma mark Objective-C private methods

@interface AudioProcessor()

@property(nonatomic, strong) NSMutableData *buffer;
@property(nonatomic) int sampleCount;
@property(nonatomic) dispatch_queue_t serialQueue;
@property (readwrite) BOOL isActive;

-(void)processBuffer: (AudioBufferList*) audioBufferList;
// error managment
-(void)hasError:(int) statusCode file:(char*) file line:(int)line;


@end

#pragma mark Recording callback

static OSStatus recordingCallback(void *inRefCon, 
                                  AudioUnitRenderActionFlags *ioActionFlags, 
                                  const AudioTimeStamp *inTimeStamp, 
                                  UInt32 inBusNumber, 
                                  UInt32 inNumberFrames, 
                                  AudioBufferList *ioData) {
	
	// the data gets rendered here
    AudioBuffer buffer;
    
    // a variable where we check the status
    OSStatus status;
    
    /**
     This is the reference to the object who owns the callback.
     */
    AudioProcessor *audioProcessor = (__bridge AudioProcessor*) inRefCon;
    
    /**
     on this point we define the number of channels, which is mono
     for the iphone. the number of frames is usally 512 or 1024.
     */
    buffer.mDataByteSize = inNumberFrames * 2; // sample size
    buffer.mNumberChannels = 1; // one channel
	buffer.mData = malloc( inNumberFrames * 2 ); // buffer size
	
    // we put our buffer into a bufferlist array for rendering
	AudioBufferList bufferList;
	bufferList.mNumberBuffers = 1;
	bufferList.mBuffers[0] = buffer;
    
    // render input and check for error
    status = AudioUnitRender([audioProcessor audioUnit], ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, &bufferList);
    [audioProcessor hasError:status file:__FILE__ line:__LINE__];
    
	// process the bufferlist in the audio processor
    [audioProcessor processBuffer:&bufferList];
	
    // clean up the buffer
	free(bufferList.mBuffers[0].mData);
	
    return noErr;
}

#pragma mark objective-c class

@implementation AudioProcessor
@synthesize audioUnit, audioBuffer, gain;

-(AudioProcessor*)init
{
    self = [super init];
    if (self) {
        gain = 0;
        self.serialQueue = dispatch_queue_create("com.bracketforge.audio", DISPATCH_QUEUE_SERIAL);

        [self initializeAudio];
    }
    return self;
}

- (void)checkPermissions {
    AVAudioSession *session = [AVAudioSession sharedInstance];

    // user already granter permissions
    if (session.recordPermission == AVAudioSessionRecordPermissionGranted) {
        return;
    }

    [session requestRecordPermission:^(BOOL granted) {
        NSLog(@"AUDIO: permissions granted: %i", granted);
    }];

}

-(void)initializeAudio
{    
    OSStatus status;
	
	// We define the audio component
	AudioComponentDescription desc;
	desc.componentType = kAudioUnitType_Output; // we want to ouput
	desc.componentSubType = kAudioUnitSubType_VoiceProcessingIO; // we want in and ouput
	desc.componentFlags = 0; // must be zero
	desc.componentFlagsMask = 0; // must be zero
	desc.componentManufacturer = kAudioUnitManufacturer_Apple; // select provider
	
	// find the AU component by description
	AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
	
	// create audio unit by component
	status = AudioComponentInstanceNew(inputComponent, &audioUnit);
    
	[self hasError:status file:__FILE__ line:__LINE__];
	
    // define that we want record io on the input bus
    UInt32 flag = 1;
	status = AudioUnitSetProperty(audioUnit, 
								  kAudioOutputUnitProperty_EnableIO, // use io
								  kAudioUnitScope_Input, // scope to input
								  kInputBus, // select input bus (1)
								  &flag, // set flag
								  sizeof(flag));
	[self hasError:status file:__FILE__ line:__LINE__];

    // define that we DON'T on io on the output bus
    UInt32 disableFlag = 0;
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioOutputUnitProperty_EnableIO, // use io
                                  kAudioUnitScope_Output, // scope to output
                                  kOutputBus, // select output bus (0)
                                  &disableFlag, // set flag
                                  sizeof(disableFlag));
    [self hasError:status file:__FILE__ line:__LINE__];

	/* 
     We need to specifie our format on which we want to work.
     We use Linear PCM cause its uncompressed and we work on raw data.
     for more informations check.
     
     We want 16 bits, 2 bytes per packet/frames at 44khz 
     */
	AudioStreamBasicDescription audioFormat;
	audioFormat.mSampleRate			= SAMPLE_RATE;
	audioFormat.mFormatID			= kAudioFormatLinearPCM;
	audioFormat.mFormatFlags		= kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
	audioFormat.mFramesPerPacket	= 1;
	audioFormat.mChannelsPerFrame	= 1;
	audioFormat.mBitsPerChannel		= 16;
	audioFormat.mBytesPerPacket		= 2;
	audioFormat.mBytesPerFrame		= 2;


    // set the format on the output stream
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));

    [self hasError:status file:__FILE__ line:__LINE__];
    
    // set the format on the input stream
    status = AudioUnitSetProperty(audioUnit,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &audioFormat,
                                  sizeof(audioFormat));
    [self hasError:status file:__FILE__ line:__LINE__];

    /**
        We need to define a callback structure which holds
        a pointer to the recordingCallback and a reference to
        the audio processor object
     */
	AURenderCallbackStruct callbackStruct;
    
    // set recording callback
	callbackStruct.inputProc = recordingCallback; // recordingCallback pointer
    callbackStruct.inputProcRefCon = (__bridge void * _Nullable)(self);

    // set input callback to recording callback on the input bus
	status = AudioUnitSetProperty(audioUnit, 
                                  kAudioOutputUnitProperty_SetInputCallback, 
								  kAudioUnitScope_Global, 
								  kInputBus, 
								  &callbackStruct, 
								  sizeof(callbackStruct));
    
    [self hasError:status file:__FILE__ line:__LINE__];
	
    // reset flag to 0
	flag = 0;

	// Initialize the Audio Unit and cross fingers =)
    status = AudioUnitInitialize(audioUnit);
    [self hasError:status file:__FILE__ line:__LINE__];

    LIFELogDebug(@"AUDIO: Started");

    // workaround to allow normal sound routing, while not started record
    [self stop];
}

#pragma mark controll stream

-(void)start;
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord
             withOptions:0
                   error:nil];
    [session setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];

    [self checkPermissions];
    // start the audio unit. You should hear something, hopefully :)
    OSStatus status = AudioOutputUnitStart(audioUnit);
    if (status) {
        self.isActive = YES;
    }
    [self hasError:status file:__FILE__ line:__LINE__];

    NSLog(@"AUDIO: Start called");
}

-(void)stop;
{
    // stop the audio unit
    OSStatus status = AudioOutputUnitStop(audioUnit);
    [self hasError:status file:__FILE__ line:__LINE__];

    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback
             withOptions:0
                   error:nil];
    [session setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];

    NSLog(@"AUDIO: Stop called");
    self.isActive = NO;
}


-(void)setGain:(float)gainValue 
{
    gain = gainValue;
}

-(float)getGain
{
    return gain;
}

#pragma mark processing

-(void)processBuffer: (AudioBufferList*) audioBufferList
{
    AudioBuffer sourceBuffer = audioBufferList->mBuffers[0];

//    NSLog(@"AUDIO: source buffer size: %i", sourceBuffer.mDataByteSize);
    NSData *result = [NSData dataWithBytes:sourceBuffer.mData length:sourceBuffer.mDataByteSize];



    dispatch_async(self.serialQueue, ^{
        if (self.sampleCount == 0) {
            self.buffer = [NSMutableData data];
        }

        double sampleRate = SAMPLE_RATE;
        double bufferSize = self.buffer.length;

        [self.buffer appendData:result];
        self.sampleCount = self.sampleCount + 1;

        // send each 8 or 4 frames  - roughly 120 milliseconds
        if (bufferSize >= MAX_BUFFER_SIZE)  {
            self.sampleCount = 0;

//            NSLog(@"AUDIO: sample rate: %f, buffer size %f", sampleRate, bufferSize);
            [self.delegate audioRecorderDidCaptureAudioData:self.buffer sampleRate:sampleRate];
        }
    });
}

#pragma mark Error handling

-(void)hasError:(int) statusCode file:(char*) file line:(int)line
{
	if (statusCode) {
        NSString * text;
        switch (statusCode) {
            case kAudioUnitErr_CannotDoInCurrentContext: text = @"kAudioUnitErr_CannotDoInCurrentContext"; break;
            case kAudioUnitErr_FailedInitialization: text = @"kAudioUnitErr_FailedInitialization"; break;
            case kAudioUnitErr_FileNotSpecified: text = @"kAudioUnitErr_FileNotSpecified"; break;
            case kAudioUnitErr_FormatNotSupported: text = @"kAudioUnitErr_FormatNotSupported"; break;
            case kAudioUnitErr_Initialized: text = @"kAudioUnitErr_Initialized"; break;
            case kAudioUnitErr_InvalidElement: text = @"kAudioUnitErr_InvalidElement"; break;
            case kAudioUnitErr_InvalidFile: text = @"kAudioUnitErr_InvalidFile"; break;
            case kAudioUnitErr_InvalidOfflineRender: text = @"kAudioUnitErr_InvalidOfflineRender"; break;
            case kAudioUnitErr_InvalidParameter: text = @"kAudioUnitErr_InvalidParameter"; break;
            case kAudioUnitErr_InvalidProperty: text = @"kAudioUnitErr_InvalidProperty"; break;
            case kAudioUnitErr_InvalidPropertyValue: text = @"kAudioUnitErr_InvalidPropertyValue"; break;
            case kAudioUnitErr_InvalidScope: text = @"kAudioUnitErr_InvalidScope"; break;
            case kAudioUnitErr_NoConnection: text = @"kAudioUnitErr_NoConnection"; break;
            case kAudioUnitErr_PropertyNotInUse: text = @"kAudioUnitErr_PropertyNotInUse"; break;
            case kAudioUnitErr_PropertyNotWritable: text = @"kAudioUnitErr_PropertyNotWritable"; break;
            case kAudioUnitErr_TooManyFramesToProcess: text = @"kAudioUnitErr_TooManyFramesToProcess"; break;
            case kAudioUnitErr_Unauthorized: text = @"kAudioUnitErr_Unauthorized"; break;
            case kAudioUnitErr_Uninitialized: text = @"kAudioUnitErr_Uninitialized"; break;
            case kAudioUnitErr_UnknownFileType: text = @"kAudioUnitErr_UnknownFileType"; break;
            default: text = @"unknown error";
        }
        LIFELogError(@"AUDIO: Error Code responded \"%@\" (%d) in file %s on line %d\n", text, statusCode, file, line);
	}
}


@end
