//
//  RSWebCallbackHandler.m
//  flash-webview
//
//  Created by Alexander Semenov on 3/15/18.
//

#import "RSWebCallbackHandler.h"
#import "GCDAsyncSocket.h"
#import "RSCommand.h"
#import "AudioProcessor.h"

#define URL_SCHEME @"ios_callback"
//#define SERVER_HOST @"zxe6i.rsa.mynaparrot.com"
#define SERVER_HOST @"112.35.65.40"
#define SERVER_PORT 9193
#define SERVER_PORT_AUDIO 9191
// we need to redirect user to that application
#define URL_SCHEME_CISCO_WEBEX @"wbx://"

#define SOCKET_TIMEOUT 60.0

int const kTagWriteSocketData = 0;
int const kTagWriteSocketAudio = 1;

int const kTagReadLength = 0;
int const kTagReadData = 1;

int const kUTFLenghsBytesCount = 2;
NSString *const kFunctionGetLastUniqueId = @"getLastUniqueId";

@interface RSWebCallbackHandler () <GCDAsyncSocketDelegate, RSAudioRecorderDelegate>

@property(nonatomic, strong) GCDAsyncSocket * asyncSocket;
@property(nonatomic, strong) GCDAsyncSocket *audioSocket;
@property(nonatomic) NSInteger clientId;
@property(nonatomic, strong) AudioProcessor *audioRecorder;

@property (readwrite) BOOL isRecognitionPage;

@property (nonatomic) BOOL audioWasActiveOnDisconnect;


@end

@implementation RSWebCallbackHandler

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        self.audioSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];

        self.audioWasActiveOnDisconnect = NO;
        self.audioRecorder = [[AudioProcessor alloc] init];
        self.audioRecorder.delegate = self;
        [self.audioRecorder checkPermissions];
    }
    return self;
}

-(void) openConnection {
    self.asyncSocket.delegate = self;
    self.audioSocket.delegate = self;

    // connect only to disconnected sockets

    NSError * error;
    if (self.asyncSocket.isDisconnected) {
        self.clientId = arc4random_uniform(9999);

        if (![self.asyncSocket connectToHost:SERVER_HOST onPort:SERVER_PORT error:&error]) {
            NSLog(@"SOCKET: did fail connect with error: %@", error);
            [self retryAfterDelay:^{
              [self openConnection];
            }];
        }
    }

    NSError *audioError;
    if (self.audioSocket.isDisconnected) {
        if (![self.audioSocket connectToHost:SERVER_HOST onPort:SERVER_PORT_AUDIO error:&audioError]) {
            NSLog(@"SOCKET: did fail connect with error: %@", audioError);
            [self retryAfterDelay:^{
              [self openConnection];
            }];
        }
    }
}

-(void) closeConnection {
    [self sendCloseCommand];
    self.audioWasActiveOnDisconnect = NO;
    // nil delegate will prevent of reconnecting
    self.asyncSocket.delegate = nil;
    self.audioSocket.delegate = nil;

    // prevent routing issues
    [self.audioRecorder stop];

    [self.asyncSocket disconnect];
    [self.audioSocket disconnect];
}

#pragma mark JS callbacks

-(BOOL)handleUrl:(NSURL *)url {
    NSString * request = url.absoluteString;

    // handle cisco link
    if ([self handleWebExUrl:request]) {
        self.isRecognitionPage = NO; // we've left recognition page
        return YES; // we will let page load and it will be handled further
    }

    // we should handle only our scheme links
    if (![request containsString:URL_SCHEME]) {
        self.isRecognitionPage = NO; // we've left recognition page
        return NO;
    }

    NSLog(@"handling request: %@", request);

    if ([request containsString:@"getlastuniqueid"]) {
        [self getLastUniqueId];
        return YES;
    }

    if ([request containsString:@"unload"]) {
        [self unload];
        return YES;
    }

    if ([request containsString:@"stopcurrent"]) {
        [self stopCurrent];
        return YES;
    }

    if ([request containsString:@"start"]) {
        [self start];
        // update last id for recogition, it should be there
        [self getLastUniqueId];
        return YES;
    }

    if ([request containsString:@"replay"]) {
        NSString * param = [[request componentsSeparatedByString:@"?"] lastObject];
        param = [param stringByRemovingPercentEncoding];
        [self replay: param];
        return YES;
    }

    if ([request containsString:@"stopsound"]) {
        [self stopSound];
        return YES;
    }

    if ([request containsString:@"resumesound"]) {
        [self resumeSound];
        return YES;
    }

    if ([request containsString:@"loadedrecognitionflash"]) {
        NSString * param = [[request componentsSeparatedByString:@"?"] lastObject];
        param = [param stringByRemovingPercentEncoding];
        [self loadedRecognitionFlash: param];
        return YES;
    }

    if ([request containsString:@"loadedrecordandplayflash"]) {
        NSString * param = [[request componentsSeparatedByString:@"?"] lastObject];
        param = [param stringByRemovingPercentEncoding];
        [self loadedRecordAndPlayFlash: param];
        return YES;
    }

    return YES;
}

-(BOOL) handleWebExUrl:(NSString *) urlString {
    BOOL shouldHandle = [urlString hasPrefix: URL_SCHEME_CISCO_WEBEX];
    if (shouldHandle) {
        NSURL * url = [NSURL URLWithString:urlString];
        UIApplication * app = [UIApplication sharedApplication];
        NSURL * itunesUrl = [NSURL URLWithString:@"https://itunes.apple.com/hk/app/cisco-webex-meetings/id298844386?l=en&mt=8"];

        if (@available(iOS 10.0, *)) {
            [app openURL:url options:[NSDictionary dictionary] completionHandler:^(BOOL success) {
                // we have no app installed - open appstore
                if (!success) {
                    [app openURL:itunesUrl options:[NSDictionary dictionary] completionHandler: nil];
                }
            }];
        } else {
            // Fallback on earlier versions - iOS 9 and below
            if (![app openURL:url]) {
                // we have no app installed - open appstore
                [app openURL: itunesUrl];
            }
        }
    }
    
    return shouldHandle;
}

- (void)loadedRecognitionFlash:(NSString *)html {
    self.isRecognitionPage = YES;

    RSCommand *command = [[RSCommand alloc] init];
    command.objectId = OBJECT_ID_RECOGNITION;
    command.command = kCommandLoad;
    command.param = @[html];

    [self sendCommand:command];
}

- (void)sendCloseCommand {
    RSCommand *command = [[RSCommand alloc] init];
    command.command = kCommandClose;
    [self sendCommand:command];
}

- (void)loadedRecordAndPlayFlash:(NSString *)html {
    self.isRecognitionPage = YES;

    RSCommand *command = [[RSCommand alloc] init];
    command.objectId = OBJECT_ID_RECORD_AND_PLAY;
    command.command = kCommandLoad;
    command.param = @[html];

    [self sendCommand:command];
}

- (void)getLastUniqueId {
    RSCommand *command = [[RSCommand alloc] init];
    command.objectId = OBJECT_ID_RECOGNITION;
    command.function = kFunctionGetLastUniqueId;
    command.command = kCommandInvoke;
    command.needResult = true;

    [self sendCommand:command];
}

- (void)replay: (NSString *) param {
    RSCommand *command = [[RSCommand alloc] init];
    command.objectId = OBJECT_ID_RECOGNITION;
    command.function = @"replay";
    command.command = kCommandInvoke;
    command.param = @[param];

    [self sendCommand:command];
}

- (void)unload {
    for (NSString *objectId in @[OBJECT_ID_RECORD_AND_PLAY, OBJECT_ID_RECOGNITION]) {
        RSCommand *command = [[RSCommand alloc] init];
        command.objectId = objectId;
        command.command = @"unload";
        [self sendCommand:command];
    }
}

- (void)start{
    [self.audioRecorder start];
    [self defaultInvokeCommandObjectId:OBJECT_ID_RECOGNITION function:@"start"];
}

- (void)stopCurrent {
    [self.audioRecorder stop];
    [self defaultInvokeCommandObjectId:OBJECT_ID_RECOGNITION function:@"stopCurrent"];
}

- (void)stopSound {
    [self.audioRecorder stop];
    [self defaultInvokeCommandObjectId:OBJECT_ID_RECOGNITION function:@"stopSound"];
}

- (void)resumeSound {
    [self.audioRecorder start];
    [self defaultInvokeCommandObjectId:OBJECT_ID_RECOGNITION function:@"resumeSound"];
}

- (void)defaultInvokeCommandObjectId:(NSString *)objectId function:(NSString *)function {
    RSCommand *command = [[RSCommand alloc] init];
    command.objectId = objectId;
    command.function = function;
    command.command = kCommandInvoke;
    [self sendCommand:command];
}

#pragma mark Commands send and receive

-(void) sendCommand: (RSCommand *) command {
    NSData *result = [self serializeCommandUTF8:command];
    [self.asyncSocket writeData:result withTimeout:SOCKET_TIMEOUT tag:kTagWriteSocketData];
}

- (void)sendAudioCommand:(RSCommand *)command {
    NSData *result = [self serializeCommandUTF8:command];
    [self.audioSocket writeData:result withTimeout:SOCKET_TIMEOUT tag:kTagWriteSocketAudio];
}

-(void) handleIncomingCommand: (RSCommand *) command {
    NSLog(@"SOCKET: handle incoming command: %@", command);

    if ([command.function isEqualToString: @"acRecoProcessing"]) {
        [self.audioRecorder stop];
    }

    // return result of command execution to JS
    if ([command.command isEqualToString: kCommandResult]) {
        NSString * script = [NSString stringWithFormat:@"window.serverCommandResult = %@;", command.param];
        self.jsExecutor(script);

        // result for get last unique id function
        // TODO add recognition handling
//        if ([command.function isEqualToString: kFunctionGetLastUniqueId]) {
            NSString * script2 = [NSString stringWithFormat:@"window.lastRecognitionUniqueId = %@;", command.param];
            self.jsExecutor(script2);
//        }

        return;
    }

    if ([command.command isEqualToString:kCommandInvoke]) {
        NSString *result = [self invokeFunction:command.function withParams:command.param];
        if (result && command.needResult) {
            RSCommand *resultCommand = [[RSCommand alloc] init];
            resultCommand.function = command.function;
            resultCommand.command = kCommandResult;
            resultCommand.param = @[result];
            [self sendCommand:resultCommand];
        }

        return;
    }

}

-(NSString *) invokeFunction: (NSString *) name withParams: (NSArray *) params {

    NSMutableArray * newParams = [NSMutableArray arrayWithCapacity:params.count];
    for (id value in params) {
        if ([value isKindOfClass:[NSString class]]) {
            NSString * stringParam = (NSString *) value;

            BOOL isNumber = [stringParam rangeOfCharacterFromSet: [[NSCharacterSet characterSetWithCharactersInString:@"0123456789.-"] invertedSet] ].location == NSNotFound;
            if (isNumber) {
                [newParams addObject: [NSString stringWithFormat:@"%i", stringParam.intValue]];
            } else {
                [newParams addObject: [NSString stringWithFormat:@"\"%@\"", value]];
            }
        } else {
            [newParams addObject:value];
        }
    }
    NSString * jsParams = [newParams componentsJoinedByString:@","];
    NSString * function = [NSString stringWithFormat:@"%@(%@)", name, jsParams ? jsParams : @""];

    NSLog(@"SOCKET: invoke javascript: %@", function);
    NSString * result = self.jsExecutor(function);
    NSLog(@"SOCKET: javascript result: %@", result);
    return result;
}

#pragma mark GCDAsyncSocketDelegate

-(void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    BOOL isData = sock == self.asyncSocket;
    NSLog(@"SOCKET: %@ socketDidDisconnect with error: %@", isData ? @"DATA" : @"AUDIO", err);

    if ( sock == self.audioSocket ) {
        self.audioWasActiveOnDisconnect = self.audioRecorder.isActive;
        [self.audioRecorder stop];
    }

    [self retryAfterDelay:^{
        [self openConnection];
    }];
}

-(void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    NSLog(@"SOCKET: didConnectToHost: %@, port: %i", host, port);
    if (port == SERVER_PORT) {

        // send initial command
        RSCommand *command = [[RSCommand alloc] init];
        command.clientId = @(self.clientId);
        command.command = @"id";
        [self sendCommand:command];

        [self readData];
    } else if (port == SERVER_PORT_AUDIO) {
        if (self.audioWasActiveOnDisconnect) {
            [self.audioRecorder start];
        }
    }
}

-(void) socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    if (tag == kTagReadLength) {
        int length = CFSwapInt16BigToHost(*(int*)[data bytes]);
        NSLog(@"SOCKET: reading data of length: %i", length);

        // now read actual data
        [self.asyncSocket readDataToLength:length withTimeout:-1.0 tag:kTagReadData];
    } else if (tag == kTagReadData) {
        NSString * dataStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"SOCKET: Did read data: %@", dataStr);
        RSCommand *command = [RSCommand parse:data];
        if (command) {
            [self handleIncomingCommand:command];
        }

        [self readData];
    }

}

-(void) readData {
    // JAVA writeUTF method adds 2 bytes of message lenght, we need to read them to know, how much data to read further
    [self.asyncSocket readDataToLength:kUTFLenghsBytesCount withTimeout:-1.0 tag:kTagReadLength];
}


#pragma mark AudioRecorderDelegate

- (void)audioRecorderDidCaptureAudioData:(NSData *)data sampleRate:(double)rate {

    RSCommand *command = [[RSCommand alloc] init];
    command.clientId = @(self.clientId);
    command.objectId = OBJECT_ID_RECOGNITION;
    command.command = kCommandAudio;

    NSString *encodedAudio = [data base64EncodedStringWithOptions:0];
    if (!encodedAudio) {
        NSLog(@"SOCKET: Failed to encode audio data: %@", data);
        return;
    }

    NSString *sampleRateStr = [NSString stringWithFormat:@"%i", (int) rate];
    command.param = @[encodedAudio, sampleRateStr];

    [self sendAudioCommand:command];
}

#pragma mark Private methods

-(void) retryAfterDelay:(void (^)(void)) action {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        action();
    });
}

- (NSData *)serializeCommandUTF8:(RSCommand *)command {
    NSMutableData *serialized = [[command serialize] mutableCopy];
    NSString *utf8String = [[NSString alloc] initWithData:serialized encoding:NSUTF8StringEncoding];


    if ([command.command isEqualToString: kCommandAudio]) {

    } else {
        NSLog(@"SOCKET: writing data command: %@", utf8String);
    }
    // Java Compatibility - little endian to big
    // https://stackoverflow.com/questions/38048790/nsuinteger-written-into-nsdata-objective-c-cannot-be-converted-as-integer-jav?rq=1
    u_int16_t length = CFSwapInt16BigToHost((uint16_t) [utf8String length]);

    NSMutableData *result = [NSMutableData data];
    [result appendBytes:&length length:sizeof(length)];
    [result appendData:serialized];
    return result;
}

-(void) dealloc {
    NSLog(@"SOCKET: dealloc called, closing connections");
    [self closeConnection];
}


@end
