//
//  RosettaURLProtocol.m
//  FlashWebView
//
//  Created by Alexander Semenov on 3/12/18.
//  Copyright © 2018 Alexander Semenov. All rights reserved.
//

#import "RosettaURLProtocol.h"
#import "WebViewHandler.h"

@interface RosettaURLProtocol()

@property(strong, nonatomic) NSString *response;

@end


@implementation RosettaURLProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request {
//    static NSUInteger requestCount = 0;
//    NSLog(@"Request #%lu: URL = %@", (unsigned long)requestCount++, request.URL.absoluteString);

    return [[WebViewHandler sharedInstance] interceptHttpRequest:request] != nil;
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {

    NSLog(@"%s %@", __func__, [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding]);
    NSLog(@"%s %@", __func__, [request valueForHTTPHeaderField:@"field"]);

    return request;
}

- (instancetype)initWithRequest:(NSURLRequest *)request cachedResponse:(NSCachedURLResponse *)cachedResponse client:(id<NSURLProtocolClient>)client {
    self = [super initWithRequest:request cachedResponse:cachedResponse client:client];
    if ( self ) {
        self.response = [[WebViewHandler sharedInstance] interceptHttpRequest:request];
    }
    return self;
}

- (void)startLoading {

    // simu get data
    NSString *htmlString = self.response;

    NSURLResponse *response = [[NSURLResponse alloc] initWithURL:[NSURL URLWithString:@"https://github.com"] MIMEType:@"script/js" expectedContentLength:[htmlString dataUsingEncoding:NSUTF8StringEncoding].length textEncodingName:@"utf-8"];

    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
    [self.client URLProtocol:self didLoadData:[htmlString dataUsingEncoding:NSUTF8StringEncoding]];
    [self.client URLProtocolDidFinishLoading:self];
}

- (void)stopLoading {

}

@end
