//
//  AudioProcessor.h
//  MicInput
//
//  Created by Stefan Popp on 21.09.11.
//  Copyright 2011 http://http://www.stefanpopp.de/2011/capture-iphone-microphone//2011/capture-iphone-microphone/ . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@protocol RSAudioRecorderDelegate
@required

- (void)audioRecorderDidCaptureAudioData:(NSData *)data sampleRate:(double)rate;

@end

@interface AudioProcessor : NSObject
{
    // Audio unit
    AudioComponentInstance audioUnit;
    
    // Audio buffers
//    AudioBuffer audioBuffer;

    // gain
    float gain;
}

@property (readonly) AudioBuffer audioBuffer;
@property (readonly) AudioComponentInstance audioUnit;
@property (nonatomic) float gain;
@property (readonly) BOOL isActive;

@property(weak) id <RSAudioRecorderDelegate> delegate;

-(AudioProcessor*)init;

-(void)checkPermissions;

// control object
-(void)start;
-(void)stop;

// gain
-(void)setGain:(float)gainValue;
-(float)getGain;

@end
