//
//  WebViewHandler.m
//  FlashWebView
//
//  Created by Alexander Semenov on 3/12/18.
//  Copyright © 2018 Alexander Semenov. All rights reserved.
//

#import "WebViewHandler.h"
#import "RosettaURLProtocol.h"
#import <WebKit/WebKit.h>
#import "PodAsset.h"
#import "RSWebCallbackHandler.h"

@interface WebViewHandler () <UIWebViewDelegate>

@property(strong, nonatomic, readwrite) UIWebView *webView;
@property(strong, nonatomic, readwrite) NSDictionary *replaceMap;
@property(strong, nonatomic) RSWebCallbackHandler *callbackHandler;

@end

@implementation WebViewHandler

+ (WebViewHandler *)sharedInstance {
    static WebViewHandler *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      instance = [[WebViewHandler alloc] init];
      [instance prefillMap];
      [NSURLProtocol registerClass:[RosettaURLProtocol class]];
      instance.callbackHandler = [[RSWebCallbackHandler alloc] init];

      __weak WebViewHandler * weakInstance = instance;
      instance.callbackHandler.jsExecutor = ^NSString *(NSString *script) {
         if (weakInstance == nil) {
            @throw NSInternalInconsistencyException;
        }
        return [weakInstance.webView stringByEvaluatingJavaScriptFromString:script];
      };
    });
    return instance;
}

- (void)setupWebViewContainer:(WKWebView *)view url:(NSString *)url {
    [self unsubscribeFromNotifications];
    // important - not to have loaded two pages simultaneously
    [view stopLoading];
    NSLog(@"Setup webVIew with:  %@, url: %@", view, url);

    if (self.webView == nil) {
        self.webView = [[UIWebView alloc] init];
    }

    [self setupWebView: self.webView];
    NSURL * neededUrl = [NSURL URLWithString: url];
    [self.webView loadRequest:[NSURLRequest requestWithURL:neededUrl]];

    // to ensure our webview not added to several views simultaneously
    [self.webView removeFromSuperview];

    [view.superview addSubview:self.webView];
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (@available(iOS 9.0, *)) {
        [self.webView.topAnchor constraintEqualToAnchor:view.superview.topAnchor].active = YES;
        [self.webView.topAnchor constraintEqualToAnchor:view.superview.topAnchor].active = YES;
        [self.webView.bottomAnchor constraintEqualToAnchor:view.superview.bottomAnchor].active = YES;
        [self.webView.leftAnchor constraintEqualToAnchor:view.superview.leftAnchor].active = YES;
        [self.webView.rightAnchor constraintEqualToAnchor:view.superview.rightAnchor].active = YES;
    }

    self.webView.allowsInlineMediaPlayback = YES;
    self.webView.frame = view.superview.bounds;

    [view.superview bringSubviewToFront:self.webView];

    // lifecycle

    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    [center addObserver:self  selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification  object:nil];
    [center addObserver:self selector:@selector(appWillGoForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [center addObserver:self selector:@selector(appWillGoBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

-(void) unsubscribeFromNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)setupWebView:(UIWebView *)webView {

    self.webView = webView;
    self.webView.delegate = self;

    self.webView.scalesPageToFit = YES;

    self.webView.scrollView.minimumZoomScale = 0.0;
    self.webView.scrollView.maximumZoomScale = 1.0;

    [self.callbackHandler openConnection];
}

-(void) restart {
    // restart connection
    [self.callbackHandler closeConnection];

    [self.callbackHandler openConnection];
    [self.webView reload];

}

-(void) orientationChanged: (NSNotification *) notification {
    // don't need for now - we need to find other way to resize webview
//    [self.webView reload];
}

#pragma mark JS interception

- (void)prefillMap {
    self.replaceMap = @{
            @"https://ss4001.rosettastone.cn/221270--8920100/Scripts/TellMeMore/SystemManagement/Verification/VerificationPage.js?22.23.11.0": @"flash_verification",
            @"https://cnpro1.rosettastone.cn/ScriptResource.axd?d=mCjoQych5f-x-kg8AJ-e5xrZ_MLsAQjNi86tYvrF18htPmSMjPTRvHpRzEF8VgtA3mMnzcxEFVyJUY9-nLJV1LfwkOnCCfiBVTVlnOSTTjcmAimqQaHf5mG1G4wbUxL-Hsd7ZMDXPM4A3WDeJ1MGfhxnN8oLIJktPuX0UkQ_eOfZ-abxjFuJIkkgdok95cIVDumS43q16g8C2NGylgYpIAGWztk1&t=ffffffffe9f9e9fe": @"speech_recognition",
            @"https://cnpro1.rosettastone.cn/ScriptResource.axd?d=TJkTlAsdlbjOWmcsv_lGdq2HvhdBvXvGEPI4sEGsXM2wq_LrCo8evg8C8TBazXaHI485CdCAt9piunZ0q5BqvJgw5q6NkoTV_KCtTu7ookGiERRopYAoVbtcI46HZFTLusFomoJFBKmIGFrDyaEedwATge7z-jit2t--gPI7DusR4Pqr0&t=ffffffffc955a50e": @"sound_player",
            @"https://cnpro1.rosettastone.cn/ScriptResource.axd?d=lyhlO8llAbCxDqzJcclL1VXJrw9aj2u8C5T1N_C7I7Vs6XjD_p_r6YbqddaGf4lyH6O16P9cfzcNhPBSTBH8vtIx1NL-znQnY4t-R7W3L2Hz6E6Lh-U6APrtW5ePQ9hp_cnTWF6buGikfL-3QVWulqliIPtNYnz_kpADjx4xrbJPrGigrrB-hG_z0pDLRD7KwT_W6g2&t=62c07da5": @"ac_callbacks"
    };
}

- (NSString *)interceptHttpRequest:(NSURLRequest *)request {
    NSString *absoluteURL = request.URL.absoluteString;

    NSString *replaceScript = [WebViewHandler sharedInstance].replaceMap[absoluteURL];
    if (replaceScript) {
        NSLog(@"EEEEE: found script!: %@", replaceScript);

        NSBundle *bundle = [PodAsset bundleForPod:@"flash-webview"];
        NSString *path = [bundle pathForResource:replaceScript ofType:@"js"];

        NSString *jsContent = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"EEEEE: script path!: %@", path);
        return jsContent;
    }
    return nil;

}

#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // if we handled url - we should skip loading further - it's our callback
    if ([self.callbackHandler handleUrl:request.URL]) {
        return NO;
    }
    return YES;
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    // override zoom for iPhones only
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        NSString* js =
        @"var meta = document.createElement('meta'); " \
        "meta.setAttribute( 'name', 'viewport' ); " \
        "meta.setAttribute( 'content', 'width = device-width, initial-scale = 0.652, minimum-scale=0.5,maximum-scale=10.0 user-scalable = yes' ); " \
        "document.getElementsByTagName('head')[0].appendChild(meta)";
        [webView stringByEvaluatingJavaScriptFromString: js];
    }
    
    // video inlining
    NSString* js =
    @"var vids = document.getElementsByTagName('video');" \
    "for( var i = 0; i < vids.length; i++ ) { "\
    "    vids.item(i).setAttribute('playsinline',''); "\
    "}";
    [webView stringByEvaluatingJavaScriptFromString: js];

    // webex stuff
    if ([webView.request.URL.absoluteString containsString:@"callme.do"] && [webView.request.URL.absoluteString containsString:@"webex"]) { // hack to prevent loading blank page from webex
          NSLog(@"handle URL: got webex url, going back!: %@", webView.request.URL.absoluteString );
        // for some reason go back blocs further webex loading, so we need to let it load
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // we need to go back by 1 page
            [webView goBack];
        });
    }
}

#pragma mark App lifecycle

-(void)appWillGoForeground {
    [self.callbackHandler openConnection];
    // reload only on recognition pages to prevent spoiling app
    if (self.callbackHandler.isRecognitionPage) {
        [self.webView reload];
    }
}

-(void)appWillGoBackground {
    [self.callbackHandler closeConnection];

}


@end

