Type.registerNamespace('Auralog.TellMeMore.Ajax.SpeechRecognition');

// *************************************************************************************
// ENUMERATIONS
// *************************************************************************************

// Modes of SpeechRecognition
Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes = function () {
    throw Error.invalidOperation();
};

// RecoModes, should match with the c# enumeration
Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.prototype = {
    NotSet: 0,
    Dll: 1,
    ActiveX: 2,
    Server: 3,
    BuildInIOS: 4
};

// Registering The Speech Recognition Modes
Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.registerEnum("Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes");

// State of the Speech Recogntion
Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState = function () {
    throw Error.invalidOperation();
};

// RecoState, handle client side
Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.prototype = {
    Inactive: 0,
    Ready: 1,
    Recording: 2,
    Evaluating: 3
};

// Registering RecoState
Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.registerEnum("Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState");

// *************************************************************************************
// END OF ENUMERATIONS
// *************************************************************************************

// *************************************************************************************
// CONSTRUCTOR
// *************************************************************************************
Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior = function (element) {
    Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior.initializeBase(this, [element]);

    // Speech Recognition data
    this._vocabulary = null; // Vocabulary
    this._vocabularyForSets = null; // Vocabulary for SETS
    this._languageID = 4; // Language ID
    this._useTranscriptions = true; // Does the vocabulary contain transcriptions
    this._pronunciationActivity = false; // Is the activity a pronunciation activity
    this._activeXID = null; // ActiveX object ID
    this._generateCurves = false; // Indicates if we have to generate curves or not
    this._manualMode = true; // Indicates if the recognition is launched by the user or automatically
    this._displayMessages = true; // Indicates if a message is displayed when nothing is recognized
    this._threshold = 3; // Speech recognition threshold
    this._recoMode = Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.NotSet; // Default reco mode
    this._isBuiltInIOSMode = false;

    // History
    this._historyIndex = 0; // current index of the history
    this._history = new Array(); // history data

    // Rendering
    this._recoState = Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Inactive; // Initial Reco State
    this._inactiveState = "";
    this._readyState = "";
    this._recordingState = "";
    this._evaluatingState = "";
    this._tries = "";
    this._loadingImage = "";
    this._nothingRecognizedMessageManual = "";
    this._nothingRecognizedMessageAutomatic = "";
    this._enableDifficultyChoice = true;

    // Client/Server members
    this._callbackID = null;
    this._enableCallback = false;
    this._activityBehaviorId = null;

    //localization strings
    this._toleranceTitle = "";
    this._toleranceEasy = "";
    this._toleranceVeryHard = "";
    this._functioningModeTitle = "";
    this._onMode = "";
    this._offMode = "";

    // Other members
    this._highlighted = false;
    this._nbTries = 1;
    this._vuMeterSize = 73;
    this._evaluationMinimumTimeElapsed = true;
    this._subMenuToleranceOpen = false;
    this._subMenuFunctioningOpen = false;
    this._consecutiveLowScores = 0;
    this._acLoaded = true;
    this._recoServerVars = "";
    this._recoServerSwf = "";
    this._nbConsecutiveTimeouts = 5;
    this._nbDisplayTimeoutBox = 3;
    this._microSettingsVars = "";
    this._microSettingsSwf = "";
    this._isBrowserSupport = true;
    this._interval = null; // animate time intervalid
    this._currentFrame = 0; //animate current frame
};
// *************************************************************************************
// END OF CONSTRUCTOR
// *************************************************************************************

// *************************************************************************************
// INITIALIZATION
// *************************************************************************************
Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior.prototype = {

    // INITIALIZATION
    initialize: function () {
        Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior.callBaseMethod(this, 'initialize');
        var panel = $get("speechContainer");
        if (panel != null) {
            // write in hidden fields for DLL mode
            var lang = document.createElement('input');
            lang.setAttribute('id', 'RecoLanguage');
            lang.setAttribute('name', 'RecoLanguage');
            lang.setAttribute('type', 'hidden');
            lang.setAttribute('value', this._languageID);
            panel.appendChild(lang);

            var words = document.createElement('input');
            words.setAttribute('id', 'RecoWords');
            words.setAttribute('name', 'RecoWords');
            words.setAttribute('type', 'hidden');
            words.setAttribute('value', this._vocabulary);
            panel.appendChild(words);

            if (this._vocabularyForSets != null && this._vocabularyForSets != "") {
                var wordsSets = document.createElement('input');
                wordsSets.setAttribute('id', 'RecoWordsSets');
                wordsSets.setAttribute('name', 'RecoWordsSets');
                wordsSets.setAttribute('type', 'hidden');
                wordsSets.setAttribute('value', this._vocabularyForSets);
                panel.appendChild(wordsSets);
            }

            var hist = document.createElement('input');
            hist.setAttribute('id', 'RecoHistoryId');
            hist.setAttribute('name', 'RecoHistoryId');
            hist.setAttribute('type', 'hidden');
            hist.setAttribute('value', this.getHistoryCurrentIndex());
            panel.appendChild(hist);
        }

        this._isBuiltInIOSMode = IsBuiltinIpadMode();
        //judge browser support animate
        this._isBrowserSupport = this.judgeBrowserAnimate();

        // create setting menu
        this.createMenu();

        // start with the inactive state
        this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Inactive);

        if (this._isBuiltInIOSMode) {
            document.location = "protocol://clearCount/s";
        } else {
            this.acInsert();
            this.acLoaded();
        }
    },
    // *************************************************************************************
    // END OF INITIALIZATION
    // *************************************************************************************

    // *************************************************************************************
    // DESTRUCTOR
    // *************************************************************************************
    dispose: function () {
        // stop a current recognition in the ActiveX mde (to prevent from crash)
        if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
            ax_StopRecognition(this._activeXID);
        }
        this.stopHistorySound();
        location.href = "http://ios_callback_unload";
        Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior.callBaseMethod(this, 'dispose');
    },
    // *************************************************************************************
    // END OF DESTRUCTOR
    // *************************************************************************************

    // *************************************************************************************
    // ERROR AND CALLBACK
    // *************************************************************************************

    raiseEvent: function (args) {
        try {
            var id = this.get_CallbackID();
            if (this._enableCallback) {
                $find(this._activityBehaviorId).setResultFromSpeechRecognition(args);
                //this.notifyScoreUnderThreshold();
            } else
                __doPostBack(id, args);
        } catch (ex) {
        }
    },

    _onError: function (message, context) {
        alert(String.format("An unhandled exception has occurred:\r\n{0}", message));
    },

    _receiveServerData: function (arg, context) {
        context.raiseEndClientCallback(arg);
    },

    add_EndClientCallback: function (handler) {
        this.get_events().addHandler("EndClientCallback", handler);
    },

    remove_EndClientCallback: function (handler) {
        this.get_events().removeHandler("EndClientCallback", handler);
    },

    raiseEndClientCallback: function (result) {
        var handler = this.get_events().getHandler("EndClientCallback");
        if (handler) {
            handler(this, new Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionCallbackResultEventArgs(result));
        }
    },

    // *************************************************************************************
    // END OF ERROR AND CALLBACK
    // *************************************************************************************

    // *************************************************************************************
    // EVENTS SETTING
    // *************************************************************************************

    set_SpeechButtonEvents: function (value) {
        if (value) {
            this.get_SpeechButtonElement().onclick = function () { $find("SpeechRecognitionBehavior1")._operationClick(); };
            this.get_SpeechButtonElement().onmouseover = function () { $find("SpeechRecognitionBehavior1")._operationMouseOver(); };
            this.get_SpeechButtonElement().onmouseout = function () { $find("SpeechRecognitionBehavior1")._operationMouseOut(); };
            this.get_SpeechButtonElement().onmousedown = function () { $find("SpeechRecognitionBehavior1")._operationMouseDown(); };
        } else {
            this.get_SpeechButtonElement().onclick = null;
            this.get_SpeechButtonElement().onmouseover = null;
            this.get_SpeechButtonElement().onmouseout = null;
            this.get_SpeechButtonElement().onmousedown = null;
        }
    },

    set_SpeechMenuButtonEvents: function (value) {
        if (value) {
            //SpeechMenuLevel
            this.get_SpeechMenuToleranceEasyElement().onclick = function () { $find("SpeechRecognitionBehavior1")._speechMenuLevelClick(3); };
            this.get_SpeechMenuToleranceStandardElement().onclick = function () { $find("SpeechRecognitionBehavior1")._speechMenuLevelClick(4); };
            this.get_SpeechMenuToleranceHardElement().onclick = function () { $find("SpeechRecognitionBehavior1")._speechMenuLevelClick(5); };
            this.get_SpeechMenuToleranceVeryHardElement().onclick = function () { $find("SpeechRecognitionBehavior1")._speechMenuLevelClick(6); };

            //SpeechMenuVoice
            this.get_SpeechMenuAutomaticModeElement().onclick = function () { $find("SpeechRecognitionBehavior1")._speechMenuVoiceClick(false); };
            this.get_SpeechMenuManualModeElement().onclick = function () { $find("SpeechRecognitionBehavior1")._speechMenuVoiceClick(true); };

        } else {
            //control menu button
            this.get_speechControlButtonElement().onclick = null;

            //SpeechMenuLevel
            this.get_SpeechMenuToleranceEasyElement().onclick = null;
            this.get_SpeechMenuToleranceStandardElement().onclick = null;
            this.get_SpeechMenuToleranceHardElement().onclick = null;
            this.get_SpeechMenuToleranceVeryHardElement().onclick = null;

            //SpeechMenuVoice
            this.get_SpeechMenuAutomaticModeElement().onclick = null;
            this.get_SpeechMenuManualModeElement().onclick = null;

        }
    },

    // *************************************************************************************
    // END OF EVENTS SETTING
    // *************************************************************************************

    // *************************************************************************************
    // EVENTS
    // *************************************************************************************

    _operationMouseOver: function () {
        //ready(active)state and recording state share same button
        switch (this._recoState) {
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready:
                this.stopAnimate();
                this.playFrame('ReadyLight');
                break;
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording:
                //animate current frame
                this.playFrame('RecordingLight');
                break;
            default:
                break;
        }
    },

    _operationMouseOut: function () {
        //ready(active)state and recording state share same button
        switch (this._recoState) {
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready:
                this.stopAnimate();
                this.playFrame('ReadyNormal');
                break;
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording:
                this.playFrame('RecordingNormal');
                break;
            default:
                break;
        }

    },

    _operationMouseDown: function () {

        //ready(active)state and recording state share same button
        switch (this._recoState) {
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready:
                this.stopAnimate();
                this.playFrame('ReadyDeep');
                break;
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording:
                this.playFrame('RecordingDeep');
                break;
            default:
                break;
        }

    },

    _operationClick: function () {
        if (this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording) {
            this._stopClick(); //Pause
        } else {
            this._recordClick(); //Recording
        }
    },

    _recordClick: function () {
        if (this._recoState != Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording) {
            this.set_RecoState(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording);
            //inform activity
            if (this._recoMode != Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
                this.informStarted();
            }

            // Play reco beep
            if (this._isBuiltInIOSMode) {
                this._startIOSRecord();
            } else {
                bp_start(this._id);
            }

        }
    },

    android_show_toast: function (value) {
           //FIXME  Android.showToast(value);
    },

    _startIOSRecord: function () {
        var url = String.format("protocol://start/s?param1={0}&&param2={1}&&param3={2}&&recoId={3}", this._languageID, this._vocabulary, this._vocabularyForSets, this._id);
        document.location = url;
    },

    _stopClick: function () {
        if (this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording) {
            this._historyIndex--;
            var hist = $get("RecoHistoryId");
            if (hist != null) {
                hist.setAttribute('value', this.getHistoryCurrentIndex());
            }
            if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
                ax_StopRecognition(this._activeXID);
            } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
                if (window.document.acSpeechRecognitionRecoServer == null) {
                    android_show_toast("acSpeechRecognitionRecoServer == null");
                    window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
                }
                if (window.document.acSpeechRecognitionRecoServer != null) {
                    if (this._acLoaded) {
                        location.href = "http://ios_callback_stopCurrent";
                        //window.document.acSpeechRecognitionRecoServer.stopCurrent();
                    }
                }
            }
            this.set_RecoState(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);

            //inform activity
            if (this._enableCallback) {
                try {
                    $find(this._activityBehaviorId).informSpeechRecognitionStopped();
                } catch (err) {
                    // some activities does not need this information
                }
            }
        }
    },

    _speechMenuLevelClick: function (value) {

        // inform presentation that the threshold has changed
        try {
            $find(this._activityBehaviorId).informSpeechRecognitionNewThreshold(value);
        } catch (err) {
        }

        switch (value) {
            case 3:
                this.get_SpeechMenuToleranceEasyElement().className = "rsSpeechMenuLevelSelected";
                this.get_SpeechMenuToleranceStandardElement().className = "";
                this.get_SpeechMenuToleranceHardElement().className = "";
                this.get_SpeechMenuToleranceVeryHardElement().className = "";
                break;
            case 4:
                this.get_SpeechMenuToleranceEasyElement().className = "";
                this.get_SpeechMenuToleranceStandardElement().className = "rsSpeechMenuLevelSelected";
                this.get_SpeechMenuToleranceHardElement().className = "";
                this.get_SpeechMenuToleranceVeryHardElement().className = "";
                break;
            case 5:
                this.get_SpeechMenuToleranceEasyElement().className = "";
                this.get_SpeechMenuToleranceStandardElement().className = "";
                this.get_SpeechMenuToleranceHardElement().className = "rsSpeechMenuLevelSelected";
                this.get_SpeechMenuToleranceVeryHardElement().className = "";
                break;
            case 6:
                this.get_SpeechMenuToleranceEasyElement().className = "";
                this.get_SpeechMenuToleranceStandardElement().className = "";
                this.get_SpeechMenuToleranceHardElement().className = "";
                this.get_SpeechMenuToleranceVeryHardElement().className = "rsSpeechMenuLevelSelected";
                break;
        }
    },

    _speechMenuVoiceClick: function (value) {
        this._manualMode = value;
        if (value === true) {
            //ManualMode
            this.get_SpeechMenuAutomaticModeElement().className = "";
            this.get_SpeechMenuManualModeElement().className = "rsSpeechMenuVoiceSelected";
        } else {
            //Automatic
            this.get_SpeechMenuAutomaticModeElement().className = "rsSpeechMenuVoiceSelected";
            this.get_SpeechMenuManualModeElement().className = "";
        }
    },

    // *************************************************************************************
    // END OF EVENTS
    // *************************************************************************************

    // *************************************************************************************
    // RENDERING
    // *************************************************************************************


    // display the different states of the speech recognition component
    set_State: function (value) {
        this._recoState = value;

        switch (value) {
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Inactive:
                this.get_SpeechMessageElement().innerHTML = "";
                this.set_SpeechButtonEvents(false);
                this.set_SpeechMenuButtonEvents(false);
                this.set_SpeechButtonDisable();
                if (!this._isBuiltInIOSMode) {
                    this.stopAnimate();
                }
                this.playFrame('ReadyInActive');
                break;

            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready:
                this.set_SpeechButtonEvents(true);
                this.set_SpeechMenuButtonEvents(true);
                this.set_SpeechButtonActive();
                if (!this._isBuiltInIOSMode) {
                    this.stopAnimate();
                }
                this.playFrame('ReadyNormal');
                if (this._nbTries > 1) {
                    this.get_SpeechMessageElement().innerHTML = this._tries.replace("{0}", this._nbTries);
                } else {
                    this.get_SpeechMessageElement().innerHTML = "";
                }
                break;

            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording:
                this.set_SpeechButtonEvents(true);
                this.set_SpeechMenuButtonEvents(true);
                this.set_SpeechButtonActive();
                if (!this._isBuiltInIOSMode) {
                    this.stopProcessAnimate();
                    this.playReadyAnimate();
                }
                this.playFrame('RecordingNormal');
                if (this._nbTries > 1) {
                    this.get_SpeechMessageElement().innerHTML = this._tries.replace("{0}", this._nbTries);
                } else {
                    this.get_SpeechMessageElement().innerHTML = "";
                }
                break;

            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Evaluating:
                this.set_SpeechButtonEvents(false);
                this.set_SpeechMenuButtonEvents(true);
                this.set_SpeechButtonActive();
                if (!this._isBuiltInIOSMode) {
                    this.stopRadialAnimate();
                    this.playProcessAnimate();
                }
                if (this._nbTries > 1) {
                    this.get_SpeechMessageElement().innerHTML = this._tries.replace("{0}", this._nbTries);
                } else {
                    this.get_SpeechMessageElement().innerHTML = "";
                }
                // start timer
                if (this._isBuiltInIOSMode) {
                    this._evaluationMinimumTimeElapsed = true;
                } else {
                    this._evaluationMinimumTimeElapsed = false;
                    setTimeout("$find('SpeechRecognitionBehavior1').evaluationTimeElapsed();", 500);
                }
                break;

            default:
                break;
        }
    },

    //for setting menu & speech button active
    set_SpeechButtonActive: function () {
        $('#speechControl').removeClass('speechControlHide');
        $('#speechControlDisable').addClass('speechControlHide');
        $('#speechControl').hover(function () {
            $('#speechControl').removeClass('speechControl').addClass('speechControlHover');
        }, function () {
            $('#speechControl').removeClass('speechControlHover').addClass('speechControl');
        });
    },

    //for setting menu & speech button inactive
    set_SpeechButtonDisable: function () {
        $('#speechControl').addClass('speechControlHide');
        $('#speechControlDisable').removeClass('speechControlHide');
    },


    //play current frame
    playFrame: function (className) {
        $('#speechButton').removeClass('ReadyNormal ReadyDeep ReadyLight ReadyInActive RecordingNormal RecordingDeep RecordingLight').addClass(className);
    },

    stopFrame: function () {
        $('#speechButton').removeClass('ReadyNormal ReadyDeep ReadyLight ReadyInActive RecordingNormal RecordingDeep RecordingLight');

    },

    //stop previous animate
    stopAnimate: function () {
        this.stopProcessAnimate();
        this.stopRadialAnimate();
    },

    stopProcessAnimate: function () {
        if (this._interval !== null && this._interval !== undefined)
            clearInterval(this._interval);
        //to fix IE7 IE8 JqueryRotatePlugin
        if (this._isBrowserSupport == false) {
            $('#speechProcess').css('display', 'none');
        }
        $("#speechProcess").addClass('speechControlHide');
    },

    stopRadialAnimate: function () {
        var imgpath = $("#speechRadial").attr('src');
        $("#speechRadial").remove();
        var $img = $("<img id='speechRadial' class='speechControlHide' src='" + imgpath + "'/>");
        $('#speechAnimateContainer td:first ').prepend($img);
    },

    //animate
    playProcessAnimate: function () {
        //to fix IE7 IE8 JqueryRotatePlugin
        if (this._isBrowserSupport == false) {
            $('#speechProcess').css('display', 'inline-block');
        }
        $("#speechProcess").removeClass('speechControlHide');
        this.stopFrame();
        var angle = 0;
        this._interval = setInterval(function () {
            angle += 30;
            $("#speechProcess").rotate(angle);
        }, 50);

    },

    playReadyAnimate: function () {

        $('#speechRadial').removeClass('speechControlHide');
        //To fix Jquery Plugin velocity ie7 support
        if (this._isBrowserSupport == true) {
            $('#speechRadial').velocity({
                width: 80,
                height: 80
            }, 250).velocity({
                width: 60,
                height: 60
            }, 250);
        } else {

            $('#speechRadial').animate({
                width: 80,
                height: 80
            }, 250).animate({
                width: 60,
                height: 60
            }, 250);
        }
    },

    //setting menu
    createMenu: function () {
        var speechMenuArgs = new Object();
        speechMenuArgs.id = "speechControl"; //the button id
        speechMenuArgs.defaultPosition = 2; //3;
        speechMenuArgs.buttonClassName = "speechControl";
        speechMenuArgs.buttonSelectedClassName = "speechControlInActive";
        speechMenuArgs.offsetX = 19;
        speechMenuArgs.offsetY = 5;

        var menuContent = "<div><div class=\"rsSpeechMenuTitle\">" + this._toleranceTitle + "</div><div class=\"rsSpeechMenuLevel\"><div class=\"rsSpeechMenuLevelLabel\">" + this._toleranceEasy + "</div><div class=\"rsSpeechMenuLevelSlider\"><div class=\"rsSpeechMenuLevel1\"><a ";
        if (this._threshold == 3) {
            menuContent += " class=\"rsSpeechMenuLevelSelected\" ";
        }
        menuContent += " id=\"MenuToleranceEasy\"></a></div><div class=\"rsSpeechMenuLevel2\"><a ";
        if (this._threshold == 4) {
            menuContent += " class=\"rsSpeechMenuLevelSelected\" ";
        }
        menuContent += " id=\"MenuToleranceStandard\"></a></div><div class=\"rsSpeechMenuLevel3\"><a ";
        if (this._threshold == 5) {
            menuContent += " class=\"rsSpeechMenuLevelSelected\" ";
        }
        menuContent += " id=\"MenuToleranceHard\"></a></div><div class=\"rsSpeechMenuLevel4\"><a ";
        if (this._threshold == 6) {
            menuContent += " class=\"rsSpeechMenuLevelSelected\" ";
        }
        menuContent += " id=\"ToleranceVeryHard\"></a></div></div><div class=\"rsSpeechMenuLevelLabel\">" + this._toleranceVeryHard + "</div></div><div class=\"rsSpeechMenuTitle\">" + this._functioningModeTitle + "</div><div class=\"rsSpeechMenuVoice\"><div class=\"rsSpeechMenuVoiceLink\"><a ";
        if (this._manualMode == false) {
            menuContent += " class=\"rsSpeechMenuVoiceSelected\" ";
        }
        menuContent += " id=\"MenuAutomaticMode\">" + this._onMode + "</a></div><div class=\"rsSpeechMenuVoiceLink\"><a ";
        if (this._manualMode == true) {
            menuContent += " class=\"rsSpeechMenuVoiceSelected\" ";
        }
        menuContent += " id=\"MenuManualMode\">" + this._offMode + "</a></div></div></div>";

        speechMenuArgs.content = menuContent;
        initPopoverHelp(speechMenuArgs);
    },


    // hide the flash object for server-side speech recognition
    hideFlashRecoServer: function () {
        if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
            if ($get('RecoServerContainer') != null)
                $get('RecoServerContainer').style.zIndex = "0";
        }
    },

    // *************************************************************************************
    // END OF RENDERING
    // *************************************************************************************

    // *************************************************************************************
    // METHODS AVAILABLE FOR EXTERNAL CALLS
    // *************************************************************************************

    // activate the speech recognition component
    activateRecognition: function () {
        if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server && !this._acLoaded) {
            setTimeout("$find('SpeechRecognitionBehavior1').activateRecognition();", 200);
            return;
        }
        if (this._manualMode) {
            this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
        } else {
            this.forceClick();
        }
    },

    unactivateRecognition: function () {
        // stop any current recognition
        this._stopClick();
        // set state to inactive
        this.set_RecoState(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Inactive);
    },

    // method called to indicate that the score of the last recognition was under the threshold
    notifyScoreUnderThreshold: function () {
        if (this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Evaluating) {
            if (this._manualMode) {

                this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
                // fail beep
                bp_startError(this._id);
            } else {
                if (!this._pronunciationActivity)
                    this.forceClick();
                else {
                    this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
                    // fail beep
                    bp_startError(this._id);
                }
            }
        }
    },

    // method called to indicate that a wrong answer was chosen
    notifyWrongAnswer: function () {
        if (this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Evaluating) {
            this._nbTries = 1;
            if (this._manualMode) {
                this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
            } else {
                this.forceClick();
            }
        }
    },

    // method called to indicate that the good answer was chosen
    notifyGoodAnswer: function (makeInactive, newVocabulary, newVocabularyForSets) {
        if (this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Evaluating) {
            this._nbTries = 1;
            if (makeInactive) {
                this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Inactive);
            } else {
                if (this._manualMode) {
                    this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
                } else {
                    if (newVocabulary != null && newVocabulary != "") {
                        this.set_Vocabulary(newVocabulary);
                        this.set_VocabularyForSets(newVocabularyForSets);
                    }
                    this.forceClick();
                }
            }
        }
    },

    // called by the pronunciation activity when the sounds have been played and the reco can restart (automatic mode)
    notifyPronunciationActivityReady: function () {
        if ((this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready) && (!this._manualMode) && (this._pronunciationActivity)) {
            this.forceClick();
        }
    },

    // force the click on the record button (automatic mode)
    forceClick: function () {
        if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll) {

            this.set_SpeechButtonEvents(true);
            this._recordClick();

        } else {
            this._recordClick();
        }
    },

    // display the "evaluating" state
    makeEvaluating: function () {
        this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Evaluating);

        try {
            $find(this._activityBehaviorId).informSpeechRecognitionEvaluating();
        } catch (err) {
        }
    },

    // display an error message
    makeError: function () {
        try {
            $find(this._activityBehaviorId).informSpeechRecognitionError();
        } catch (err) {
        }
    },

    // method called to indicate that the minimum time for evaluating mode has elapsed
    evaluationTimeElapsed: function () {
        this._evaluationMinimumTimeElapsed = true;
    },

    // launch the recognition
    launchRecognition: function () {
        if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
            var res = ax_StartRecognition(this._id, this._vocabulary, this._languageID, this._activeXID, this._useTranscriptions);
            if (res != 0) {
                this.makeError();
            }
        } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
            if (window.document.acSpeechRecognitionRecoServer == null) {
                android_show_toast("acSpeechRecognitionRecoServer == null");
                window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
            }
            if (window.document.acSpeechRecognitionRecoServer != null) {
                this.addHistorySound("");
                if ($get('RecoServerContainer') != null)
                    $get('RecoServerContainer').style.zIndex = "200";
                    location.href = "http://ios_callback_start";
                //window.document.acSpeechRecognitionRecoServer.start();
            }
        } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll
            || this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.BuildInIOS) {
            this.addHistorySound("");
            var hist = $get("RecoHistoryId");
            if (hist != null) {
                hist.setAttribute('value', this.getHistoryCurrentIndex());
            }
        }
    },

    // add an entry in the history
    addHistorySound: function (soundPath) {
        this._history[this._historyIndex] = new Array();
        this._history[this._historyIndex]["soundPath"] = soundPath;
        this._historyIndex++;
    },

    // add an entry in the history
    addStreamName: function (streamName) {
        if ((this._historyIndex > 0) && (this._history[this._historyIndex - 1] != null)) {
            this._history[this._historyIndex - 1]["stream"] = streamName;
        }
    },

    // get the current history index
    getHistoryCurrentIndex: function () {
        return this._historyIndex;
    },

    // get the id of the sound in server-side mode
    getLastRecognitionUniqueId: function () {
        if (window.document.acSpeechRecognitionRecoServer == null) {
            window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
            android_show_toast("acSpeechRecognitionRecoServer == null");
        }
        if (window.document.acSpeechRecognitionRecoServer != null) {
            return window.lastRecognitionUniqueId
            // window.serverCommandResult = null;
            // location.href = "http://ios_callback_getLastUniqueId";
            // // TODO add some timer here
            // while (true) {
            //     if (window.serverCommandResult) {
            //         return window.serverCommandResult;
            //     }
            // }
        } else {
            return "";
        }
    },

    // method called when there is a recognition result
    setResults: function (index, score, sets, curves) {
        if (this._isBuiltInIOSMode) {
            this.launchRecognition();
        }

        var stop = false;
        if (score == 0) {
            this._historyIndex--;
            this.makeError();
            this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
            return;
        }
        if (acGetNbTimeOut() >= this._nbConsecutiveTimeouts && acGetNbTimeOutBox() < this._nbDisplayTimeoutBox) {
            try {
                acResetTimeOut();
                if (acGetNbTimeOutBox() < this._nbDisplayTimeoutBox) {
                    acTimeOutBox();
                    showSpeechFiveTimeoutDialog();
                    this._consecutiveLowScores = 0;
                }
            } catch (err) {
            }
        } else {
            if (score <= 2) {
                this._consecutiveLowScores++;
                if (this._consecutiveLowScores >= 5) {
                    try {
                        $find(this._activityBehaviorId).informSpeechRecognitionMicroError();
                        stop = true;
                    } catch (err) {
                    }
                    this._consecutiveLowScores = 0;
                }
            } else {
                this._consecutiveLowScores = 0;
            }
        }
        if ((this._historyIndex > 0) && (this._history[this._historyIndex - 1] != null)) {
            this._history[this._historyIndex - 1]["index"] = index;
            this._history[this._historyIndex - 1]["score"] = score;
            if ((this._vocabularyForSets != null) && (this._vocabularyForSets != "")) {
                if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
                    ax_StartSets(this._id, this._vocabularyForSets, this._activeXID, this._history[this._historyIndex - 1]["soundPath"]);
                } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server
                    || this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll
                    || this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.BuildInIOS) {
                    this._history[this._historyIndex - 1]["sets"] = sets;
                }
            } else {
                this._history[this._historyIndex - 1]["sets"] = "-1";
            }
            if (this._generateCurves) {
                if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
                    this._history[this._historyIndex - 1]["curves"] = ax_GetCurves(this._activeXID, this._history[this._historyIndex - 1]["soundPath"]);
                } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server
                    || this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll
                    || this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.BuildInIOS) {
                    this._history[this._historyIndex - 1]["curves"] = curves;
                } else {
                    this._history[this._historyIndex - 1]["curves"] = "";
                }
            } else {
                this._history[this._historyIndex - 1]["curves"] = "";
            }
            if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
                ax_UnloadVocabulary(this._activeXID);
            }

            this._nbTries++;
            stop = false;
            // send OnResult event
            if (!stop) {
                this.sendResultEvent("0;" + index + ";" + this._history[this._historyIndex - 1]["sets"] + ";" + score + ";" + this._history[this._historyIndex - 1]["curves"]);
            } else {
                this.set_State(Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Ready);
            }
        }
    },

    // send the result when the evaluating state is finished
    sendResultEvent: function (args) {
        if (this._evaluationMinimumTimeElapsed) {
            this.raiseEvent(args);
        } else {
            setTimeout("$find('SpeechRecognitionBehavior1').sendResultEvent('" + args + "');", 100);
        }
    },

    // set SETS result
    setResultsSets: function (index) {
        if ((this._historyIndex > 0) && (this._history[this._historyIndex - 1] != null)) {
            this._history[this._historyIndex - 1]["sets"] = index;
        }
    },

    judgeBrowserAnimate: function () {
        if ($.browser.msie && ($.browser.version == '6.0' || $.browser.version == '7.0' || $.browser.version == '8.0')) {
            return false;
        } else
            return true;
    },

    // change the sound level
    setSoundLevel: function (value) {
        if (this._isBuiltInIOSMode) {
            return;
        }

        var d = 63;
        if (this._recoState == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState.Recording) {
            if (this._recoMode != Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
                //max voice level, server:100, no server 75
                value = 1.3 * value;
            }
            switch (true) {
                case value <= 0:
                    d = 63;
                    break;
                case value <= 2:
                    d = 65;
                    break;
                case value <= 5:
                    d = 66;
                    break;
                case value <= 10:
                    d = 67;
                    break;
                case value <= 15:
                    d = 68;
                    break;
                case value <= 20:
                    d = 70;
                    break;
                case value <= 25:
                    d = 71;
                    break;
                case value <= 30:
                    d = 73;
                    break;
                case value <= 40:
                    d = 75;
                    break;
                case value <= 50:
                    d = 77;
                    break;
                case value <= 60:
                    d = 79;
                    break;
                case value <= 75:
                    d = 82;
                    break;
                default:
                    d = 84;
                    break;
            }

            if (this._isBrowserSupport == true) {
                $('#speechRadial').velocity({
                    width: d,
                    height: d
                }, 5);
            } else {
                $('#speechRadial').animate({
                    width: d,
                    height: d
                }, 5);
            }
        }
    },

    // play a sound from the history
    playHistorySound: function (index) {
        // play sound if it exists
        if (this._historyIndex > index) {
            switch (this._recoMode) {
                case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX:
                    ax_StopUserSound(this._activeXID);
                    ax_PlayUserSound(this._activeXID, this._history[index]["soundPath"]);
                    break;
                case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server:
                    if (window.document.acSpeechRecognitionRecoServer == null) {
                        android_show_toast("acSpeechRecognitionRecoServer == null");
                        window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
                    }
                    if (window.document.acSpeechRecognitionRecoServer != null) {
                        if (this._acLoaded) {
                            //window.document.acSpeechRecognitionRecoServer.replay(this._history[index]["stream"]);
                            var stream = this._history[index]["stream"]
                            location.href = "http://ios_callback_replay?" + stream;
                        }
                    }
                    break;
                case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll:
                    $get('RecoPlayHistory').className = index;
                    $get('RecoPlayHistory').click();
                    break;
                case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.BuildInIOS:
                    this.playIOSHistorySound(index);
                    break;
            }
        }
    },

    playIOSHistorySound: function (index) {
        //document.location = "protocol://play/s?" + "soundIndex=" + index + "&&recoId=" + this._id;
        document.location = "protocol://play/s?" + "soundIndex=" + index;
    },

    // stop playing a history sound
    stopHistorySound: function () {
        switch (this._recoMode) {
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX:
                ax_StopUserSound(this._activeXID);
                break;
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server:
                if (window.document.acSpeechRecognitionRecoServer == null) {
                    android_show_toast("acSpeechRecognitionRecoServer == null");
                    window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
                }
                if (window.document.acSpeechRecognitionRecoServer != null) {
                    if (this._acLoaded) {
                         location.href = "http://ios_callback_stopSound";
                        //window.document.acSpeechRecognitionRecoServer.stopSound();
                    }
                }
                break;
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll:
                this._stopClick();
                break;
            case Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.BuildInIOS:
                this.stopIOSHistorySound();
                break;
        }
    },

    stopIOSHistorySound: function () {
        document.location = "protocol://stop/s";
        this._stopClick();
    },

    // resume a history sound
    resumeHistorySound: function () {
        if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.ActiveX) {
            ax_ResumeUserSound(this._activeXID);
        } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
            if (window.document.acSpeechRecognitionRecoServer == null) {
                android_show_toast("acSpeechRecognitionRecoServer == null");
                window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
            }
            if (window.document.acSpeechRecognitionRecoServer != null) {
                if (this._acLoaded) {
                     location.href = "http://ios_callback_resumeSound";
                    //window.document.acSpeechRecognitionRecoServer.resumeSound();
                }
            }
        } else if (this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Dll
            || this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.BuildInIOS) {
            $get('RecoPlayHistory').className = "c";
            $get('RecoPlayHistory').click();
        }
    },

    // method called to indicate that the history sound finished playing
    notifySoundFinished: function () {
        try {
            $find(this._activityBehaviorId).informSpeechRecognitionHistorySoundFinished();
        } catch (err) {
        }
    },

    acLoaded: function () {
        if (!this._acLoaded && this._recoMode == Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes.Server) {
            if (window.document.acSpeechRecognitionRecoServer == null) {
                android_show_toast("acSpeechRecognitionRecoServer == null");
                window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
            }
            if (window.document.acSpeechRecognitionRecoServer != null) {
                if (window.document.acSpeechRecognitionRecoServer.resumeSound != null)
                    this._acLoaded = true;
                else
                    setTimeout("$find('SpeechRecognitionBehavior1').acLoaded();", 200);
            } else
                setTimeout("$find('SpeechRecognitionBehavior1').acLoaded();", 200);
        }
    },

    acInsert: function () {
        var path = this._recoServerSwf;
        var ac_obj = window.document.getElementById('acSpeechRecognitionRecoServer');
        this.android_show_toast(ac_obj);
        if (ac_obj == null) {
            var ac_cont = window.document.getElementById('RecoServerContainer');
            if (ac_cont != null) {

                var ac_string = '<div>';
                ac_string += '<object id="acMicroRecordAndPlay" name="acMicroRecordAndPlay" type="application/x-shockwave-flash" data="' + this._microSettingsSwf + '" width="6" height="6">';
                ac_string += '<param name="movie" value="' + this._microSettingsSwf + '" />';
                ac_string += '<param name="FlashVars" value="' + this._microSettingsVars + '" />';
                ac_string += '<param name="loop" value="false" />';
                ac_string += '<param name="allowScriptAccess" value="always" />';
                ac_string += '<param name="quality" value="high" />';
                ac_string += '<param name="wmode" value="opaque" />';
                ac_string += '<param name="menu" value="false" />';
                ac_string += '</object>';
                ac_string += '</div>';

                ac_string += '<div>';
                ac_string += '<object id="acSpeechRecognitionRecoServer" name="acSpeechRecognitionRecoServer" type="application/x-shockwave-flash" data="' + path + '" width="6" height="6">';
                ac_string += '<param name="movie" value="' + path + '" />';
                ac_string += '<param name="FlashVars" value="' + this._recoServerVars + '" />';
                ac_string += '<param name="loop" value="false" />';
                ac_string += '<param name="allowScriptAccess" value="always" />';
                ac_string += '<param name="quality" value="high" />';
                ac_string += '<param name="wmode" value="opaque" />';
                ac_string += '<param name="menu" value="false" />';
                ac_string += '</object>';
                ac_string += '</div>';

                ac_cont.innerHTML = ac_string;

                window.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
                window.acMicroRecordAndPlay = document.getElementById('acMicroRecordAndPlay');
                 location.href = "http://ios_callback_loadedrecognitionflash?" + window.document.acSpeechRecognitionRecoServer.outerHTML;
                setTimeout(function(){
                            location.href = "http://ios_callback_loadedrecordandplayflash?" + window.document.acMicroRecordAndPlay.outerHTML;
                            }, 2000);
            }
        }
    },

    informStarted: function () {
        if (this._enableCallback) {
            try {
                $find(this._activityBehaviorId).informSpeechRecognitionStarted();
            } catch (err) {
                // some activities does not need this information
            }
        }
    },

    // *************************************************************************************
    // END OF METHODS AVAILABLE FOR EXTERNAL CALLS
    // *************************************************************************************

    // *************************************************************************************
    // SPEECH RECOGNITION DOM ELEMENTS
    // *************************************************************************************
    get_SpeechSymbolElement: function () {
        return $get('speechSymbol');
    },

    get_SpeechButtonElement: function () {
        return $get('speechButton');
    },


    get_SpeechBottomElement: function () {
        return $get('speechBottom');
    },

    get_SpeechMessageElement: function () {
        return $get('speechMessage');
    },

    get_speechControlButtonElement: function () {
        return $get('speechControl');
    },

    get_speechControlDisableButtonElement: function () {
        return $get('speechControlDisable');
    },

    get_SpeechMenuToleranceEasyElement: function () {
        return $get('MenuToleranceEasy');
    },

    get_SpeechMenuToleranceStandardElement: function () {
        return $get('MenuToleranceStandard');
    },

    get_SpeechMenuToleranceHardElement: function () {
        return $get('MenuToleranceHard');
    },

    get_SpeechMenuToleranceVeryHardElement: function () {
        return $get('ToleranceVeryHard');
    },

    get_SpeechMenuAutomaticModeElement: function () {
        return $get('MenuAutomaticMode');
    },

    get_SpeechMenuManualModeElement: function () {
        return $get('MenuManualMode');
    },

    get_TriesTextContainer: function () {
        return $get('RecoTriesTextContainer');
    },

    // *************************************************************************************
    // END OF SPEECH RECOGNITION DOM ELEMENTS
    // *************************************************************************************

    // *************************************************************************************
    // PUBLIC PROPERTIES
    // *************************************************************************************

    get_CallbackID: function () {
        /// <value type="String">ID of the ClientCallBack</value>
        return this._callbackID;
    },
    set_CallbackID: function (value) {
        this._callbackID = value;
    },

    get_Vocabulary: function () {
        /// <value type="String">Vocabulary</value>
        return this._vocabulary;
    },

    set_Vocabulary: function (value) {
        this._vocabulary = value;
        this.raisePropertyChanged("Vocabulary");

        var words = $get("RecoWords");
        if (words != null) {
            words.setAttribute('value', value);
        }

        // if the vocabulary changes, set the number of tries to zero
        this._nbTries = 1;
        var triesText = this.get_TriesTextContainer();
        if (triesText != null)
            triesText.innerHTML = "";
    },

    get_VocabularyForSets: function () {
        /// <value type="String">VocabularyForSets</value>
        return this._vocabularyForSets;
    },

    set_VocabularyForSets: function (value) {
        this._vocabularyForSets = value;
        this.raisePropertyChanged("VocabularyForSets");

        var words = $get("RecoWordsSets");
        if (words != null) {
            words.setAttribute('value', value);
        }
    },

    get_ActiveXID: function () {
        /// <value type="String">ActiveXID</value>
        return this._activeXID;
    },

    set_ActiveXID: function (value) {
        this._activeXID = value;
        this.raisePropertyChanged("ActiveXID");
    },

    get_LanguageID: function () {
        return this._languageID;
    },

    set_LanguageID: function (value) {
        this._languageID = value;
        this.raisePropertyChanged("LanguageID");
    },

    get_UseTranscriptions: function () {
        return this._useTranscriptions;
    },

    set_UseTranscriptions: function (value) {
        this._useTranscriptions = value;
        this.raisePropertyChanged("UseTranscriptions");
    },

    get_PronunciationActivity: function () {
        return this._pronunciationActivity;
    },

    set_PronunciationActivity: function (value) {
        this._pronunciationActivity = value;
        this.raisePropertyChanged("PronunciationActivity");
    },

    get_GenerateCurves: function () {
        return this._generateCurves;
    },

    set_GenerateCurves: function (value) {
        this._generateCurves = value;
        this.raisePropertyChanged("GenerateCurves");
    },

    get_RecoState: function () {
        /// <value type="Auralog.TellMeMore.Ajax.SpeechRecognition.RecoState">Speech Recognition State</value>
        return this._recoState;
    },

    set_RecoState: function (value) {
        this._recoState = value;
        this.set_State(value);
        this.raisePropertyChanged("RecoState");
    },

    get_RecoMode: function () {
        /// <value type="Auralog.TellMeMore.Ajax.SpeechRecognition.RecoModes">Speech Recognition Mode</value>
        return this._recoMode;
    },

    set_RecoMode: function (value) {
        this._recoMode = value;
        this.raisePropertyChanged("RecoMode");
    },

    get_InactiveState: function () {
        return this._inactiveState;
    },

    set_InactiveState: function (value) {
        this._inactiveState = value;
        this.raisePropertyChanged("InactiveState");
    },

    get_ReadyState: function () {
        return this._readyState;
    },

    set_ReadyState: function (value) {
        this._readyState = value;
        this.raisePropertyChanged("ReadyState");
    },

    get_RecordingState: function () {
        return this._recordingState;
    },

    set_RecordingState: function (value) {
        this._recordingState = value;
        this.raisePropertyChanged("RecordingState");
    },

    get_EvaluatingState: function () {
        return this._evaluatingState;
    },

    set_EvaluatingState: function (value) {
        this._evaluatingState = value;
        this.raisePropertyChanged("EvaluatingState");
    },

    get_Tries: function () {
        return this._tries;
    },

    set_Tries: function (value) {
        this._tries = value;
        this.raisePropertyChanged("Tries");
    },

    get_NbConsecutiveTimeouts: function () {
        return this._nbConsecutiveTimeouts;
    },

    set_NbConsecutiveTimeouts: function (value) {
        this._nbConsecutiveTimeouts = value;
        this.raisePropertyChanged("NbConsecutiveTimeouts");
    },

    get_NbDisplayTimeoutBox: function () {
        return this._nbDisplayTimeoutBox;
    },

    set_NbDisplayTimeoutBox: function (value) {
        this._nbDisplayTimeoutBox = value;
        this.raisePropertyChanged("NbDisplayTimeoutBox");
    },

    get_LoadingImage: function () {
        return this._loadingImage;
    },

    set_LoadingImage: function (value) {
        this._loadingImage = value;
        this.raisePropertyChanged("LoadingImage");
    },

    get_ManualMode: function () {
        return this._manualMode;
    },

    set_ManualMode: function (value) {
        this._manualMode = value;
        this.raisePropertyChanged("ManualMode");
    },

    get_DisplayMessages: function () {
        return this._displayMessages;
    },

    set_DisplayMessages: function (value) {
        this._displayMessages = value;
        this.raisePropertyChanged("DisplayMessages");
    },

    get_NothingRecognizedMessageManual: function () {
        return this._nothingRecognizedMessageManual;
    },

    set_NothingRecognizedMessageManual: function (value) {
        this._nothingRecognizedMessageManual = value;
        this.raisePropertyChanged("NothingRecognizedMessageManual");
    },

    get_NothingRecognizedMessageAutomatic: function () {
        return this._nothingRecognizedMessageAutomatic;
    },

    set_NothingRecognizedMessageAutomatic: function (value) {
        this._nothingRecognizedMessageAutomatic = value;
        this.raisePropertyChanged("NothingRecognizedMessageAutomatic");
    },

    get_RecoServerVars: function () {
        return this._recoServerVars;
    },

    set_RecoServerVars: function (value) {
        this._recoServerVars = value;
        this.raisePropertyChanged("RecoServerVars");
    },

    get_RecoServerSwf: function () {
        return this._recoServerSwf;
    },

    set_RecoServerSwf: function (value) {
        this._recoServerSwf = value;
        this.raisePropertyChanged("RecoServerSwf");
    },

    get_MicroSettingsVars: function () {
        return this._microSettingsVars;
    },

    set_MicroSettingsVars: function (value) {
        this._microSettingsVars = value;
        this.raisePropertyChanged("MicroSettingsVars");
    },

    get_MicroSettingsSwf: function () {
        return this._microSettingsSwf;
    },

    set_MicroSettingsSwf: function (value) {
        this._microSettingsSwf = value;
        this.raisePropertyChanged("MicroSettingsSwf");
    },

    get_Threshold: function () {
        return this._threshold;
    },

    set_Threshold: function (value) {
        this._threshold = value;
        this.raisePropertyChanged("Threshold");
    },

    get_EnableCallback: function () {
        return this._enableCallback;
    },

    set_EnableCallback: function (value) {
        this._enableCallback = value;
        this.raisePropertyChanged("EnableCallback");
    },

    get_EnableDifficultyChoice: function () {
        return this._enableDifficultyChoice;
    },

    set_EnableDifficultyChoice: function (value) {
        this._enableDifficultyChoice = value;
        this.raisePropertyChanged("EnableDifficultyChoice");
    },

    get_ActivityBehaviorId: function () {
        return this._activityBehaviorId;
    },

    set_ActivityBehaviorId: function (value) {
        this._activityBehaviorId = value;
        this.raisePropertyChanged("ActivityBehaviorId");
    },

    get_ToleranceTitle: function () {
        return this._toleranceTitle;
    },

    set_ToleranceTitle: function (value) {
        this._toleranceTitle = value;
        this.raisePropertyChanged("ToleranceTitle");
    },

    get_ToleranceEasy: function () {
        return this._toleranceEasy;
    },

    set_ToleranceEasy: function (value) {
        this._toleranceEasy = value;
        this.raisePropertyChanged("ToleranceEasy");
    },

    get_ToleranceVeryHard: function () {
        return this._toleranceVeryHard;
    },

    set_ToleranceVeryHard: function (value) {
        this._toleranceVeryHard = value;
        this.raisePropertyChanged("ToleranceVeryHard");
    },

    get_FunctioningModeTitle: function () {
        return this._functioningModeTitle;
    },

    set_FunctioningModeTitle: function (value) {
        this._functioningModeTitle = value;
        this.raisePropertyChanged("FunctioningModeTitle");
    },

    get_OnMode: function () {
        return this._onMode;
    },

    set_OnMode: function (value) {
        this._onMode = value;
        this.raisePropertyChanged("OnMode");
    },

    get_OffMode: function () {
        return this._offMode;
    },

    set_OffMode: function (value) {
        this._offMode = value;
        this.raisePropertyChanged("OffMode");
    }

    // *************************************************************************************
    // END OF PUBLIC PROPERTIES
    // *************************************************************************************
};

Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior.registerClass('Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionBehavior', Sys.Extended.UI.BehaviorBase);


Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionCallbackResultEventArgs = function (result) {
    /// <summary>
    /// Event arguments for the RatingBehavior's EndClientCallback event
    /// </summary>
    /// <param name="result" type="Object">
    /// Callback result
    /// </param>
    Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionCallbackResultEventArgs.initializeBase(this);

    this._result = result;
};

Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionCallbackResultEventArgs.prototype = {
    get_CallbackResult: function () {
        /// <value type="Object">
        /// Callback result
        /// </value>
        return this._result;
    }
};
Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionCallbackResultEventArgs.registerClass('Auralog.TellMeMore.Ajax.SpeechRecognition.SpeechRecognitionCallbackResultEventArgs', Sys.EventArgs);
