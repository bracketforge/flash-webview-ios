var cb = null;
var popupContainer = null;

$(function () {
    $(".headArea").hide();
});

Sys.Application.add_load(function () {
    // check the pupil has accept user terms, 
    // if has not accpet user terms, then will stop here and display user terms pop up window
    if (window.showUserTerms.toUpperCase() == "FALSE") {
        return;
    } 
    verifyConfiguration();
    
});

function verifyConfiguration() {
    cb = $find(vpCallbackExtenderId);
    if (cb == null) {
        return;
    }

    var html5Compliant = checkHTML5Tag();
    var flashInstalled = isInstallFlash();
    var currentFlashVersion = window.getDetailFlashVersion();
    var speechRecognitionServerSideEnabled = window._isServerSideRecoModes;
    var isTouch = window.isTouchDevice();

    var parameters = new Array();
    parameters.push(html5Compliant);
    parameters.push(flashInstalled);
    parameters.push("28.0.0.126");
    parameters.push(speechRecognitionServerSideEnabled);
    parameters.push(isTouch);

    try {
        cb.callback('verifyconfiguration', parameters, refreshVerificationPage);
    }
    catch (ex) { }
}

function refreshVerificationPage(response) {
    var verificaResult = eval(response);
    var clientAction = verificaResult.ClientAction;
    //NoAction = 0, FlashInstallationRequired = 1, PlugInInstallationRequired = 2, FlashUpgradeRequired = 3, VerificationRequested = 4, NotApplicable = 5,
    if (clientAction == "1") {
        showLimitedModeAlert();
    }
    else if (clientAction == "2") {
    }
    else if (clientAction == "3") {
        showFlashInstallationAlert();
    }
    else if (clientAction == "4") {
        verifyConfiguration();
    }
    else if (clientAction == "5") {
    }
    else if (clientAction == "0") {
        if (verificaResult.IsShowNoOptimal) {
            showNoOptimalAlert();
        }
        else {
            redirectReturnURL();
        }
    }
}


// ************************* Move to CommonScript later *************************
function isInstallFlash() {
    var flashVersion = window.getDetailFlashVersion();
    if (flashVersion == "0.0.0.0") {
        return true;
    }
    else {
        return true;
    }
}

function checkHTML5Tag() {
    //Use Modernizr to detect the html5 elements supportage
    if (Modernizr.audio && Modernizr.video)
        return true;
    return false;
}

// ************** Show flash installation prompt logic ************
function redirectReturnURL() {
    window.location.href = window.returnUrl;
}

//Flash Installed but need upgrade
function showFlashInstallationAlert() {
    $(".headArea").show();
    var alertDiv = $("#FlashInstallAlertDiv");
    alertDiv.show();

    var btnPopUpFlash = $("#btnPopUpFlash");
    enabledContinueButton();
}

function enabledContinueButton() {
    var btnFlashContinue = $("#btnFlashInstallContinue");

    if (btnFlashContinue.attr('disabled').toString() == 'true'
     || btnFlashContinue.attr('disabled').toString() == 'disabled') {
        btnFlashContinue.removeAttr("disabled");
        btnFlashContinue.removeClass("rsMediumBlueButtonDisable");
        btnFlashContinue.addClass("rsMediumBlueButton");
    }
    
    btnFlashContinue.addClass("rsMediumBlueButtonDisable");

    btnFlashContinue.bind("click", redirectReturnURL);
}

//Flash not installed
function showLimitedModeAlert() {
    $(".headArea").show();
    var alertDiv = $("#LimitedModeAlertDiv");
    alertDiv.show();

    enabledLimitedModeContinueButton();
}

function enabledLimitedModeContinueButton() {
    var btnLimitedModeContinue = $("#btnLimitedModeContinue");

    if (btnLimitedModeContinue.attr('disabled').toString() == 'true'
     || btnLimitedModeContinue.attr('disabled').toString() == 'disabled') {
        btnLimitedModeContinue.removeAttr("disabled");
        btnLimitedModeContinue.removeClass("rsMediumBlueButtonDisable");
        btnLimitedModeContinue.addClass("rsMediumBlueButton");
    }

    btnLimitedModeContinue.bind("click", redirectReturnURL);
}


function showNoTestAndNoSpeechRecognitionAvailableAlert() {
    var alertDiv = $("#NoTestAndSpeechDiv");
    alertDiv.show();

    var btnNoTestAndRecoAlertContinue = $("#btnNoTestAndRecoAlertContinue");
    btnNoTestAndRecoAlertContinue.bind("click", function () { redirectReturnURL(); });
}

function reloadPage() {
    window.location.reload();
}

function showNoOptimalAlert() {
    $("#NoOptimalAlertDiv").show();
    $("#btnNoOptimalContinue").bind("click", function () {
        redirectReturnURL();
    });
}