function sp_insert(path) {
    if (IsUseHtml5MediaPlayerMode())
    {
        //if SpeechRecognition not enabled  return, else continue
        if (!IsSpeechRecognitionEnabled())
            return;
    }


    var sp_obj = window.document.getElementById('SoundPlayer');
    if (sp_obj == null) {
        var flashvars = false;
        var params = {
            menu: "false",
            loop: "false",
            quality: "high",
            wmode: "opaque",
            allowScriptAccess: "always",
            movie: path
        };
        var attributes = {
            id: "SoundPlayer",
            name: "SoundPlayer"
        };
        window.swfobject.embedSWF(path, "SoundContainer", "6", "6", "6.0.0", "http://get.adobe.com/flashplayer/", flashvars, params, attributes);
        window.SoundPlayer = document.getElementById('SoundPlayer');
    }
}

function sp_play(soundStream, behaviorId) {
    if (IsUseHtml5MediaPlayerMode()) {
        return sp_play_html5(soundStream, behaviorId, false); //Play the sound by html5
    }
    else {
        return sp_play_flash(soundStream, behaviorId); //Play the sound by flash
    }
}

function sp_stop() {
    if (IsUseHtml5MediaPlayerMode()) {
        sp_stop_html5(); //Stop the sound by html5
    }
    else {
        sp_stop_flash(); //Stop the sound by flash
    }
}

function sp_play_flash(soundStream, behaviorId) {
    try {
        var sp_obj = window.document.getElementById('SoundPlayer');
        if (sp_obj != null) {
            if (sp_obj.PlaySound == null) {
                var sp_i = 0;
                while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayer")) {
                    sp_i++;
                }
                if (sp_obj.childNodes[sp_i].name == "SoundPlayer") sp_obj = sp_obj.childNodes[sp_i];
            }
            sp_obj.PlaySound(soundStream, behaviorId);
        }
        return true;
    }
    catch (ex) { return false; }
}

function sp_stop_flash() {
    try {
        var sp_obj = window.document.getElementById('SoundPlayer');
        if (sp_obj != null) {
            if (sp_obj.StopSound == null) {
                var sp_i = 0;
                while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayer")) {
                    sp_i++;
                }
                if (sp_obj.childNodes[sp_i].name == "SoundPlayer") sp_obj = sp_obj.childNodes[sp_i];
            }
            sp_obj.StopSound();
        }
    }
    catch (ex) { }
}

function sp_ready(behaviorId) {
    try {
        $find(behaviorId).sound_Ready();
    }
    catch (ex) { }
}

function sp_sound_finished(behaviorId) {
    $find(behaviorId).receivedMediaDiffusionEnded();
}

function sp_sound_started(behaviorId) {
    try {
        $find(behaviorId).sound_started();
    }
    catch (ex) { }
}

function sp_insert_free_dialog(path) {
    if (IsUseHtml5MediaPlayerMode()) {
        var soundPlayerContainer = $("#SoundContainerFreeDialog");
        var progressHtml = "<div class=\"soundPlayerContainer\"><div id=\"soundLoadingBar\" class=\"loadingbar\">"
            + "<span id=\"soundProgressBar\" class=\"loadingPercent\" style=\"width: 0%;\"></span></div>"
            + "<div class=\"loadingTime\"><span id=\"soundDuration\"></span></div></div>";
        soundPlayerContainer.html(progressHtml);
    } else {
        var sp_obj = window.document.getElementById('SoundPlayerFreeDialog');
        if (sp_obj == null) {
            var flashvars = false;
            var params = {
                menu: "false",
                loop: "false",
                quality: "high",
                wmode: "opaque",
                allowScriptAccess: "always",
                movie: path
            };
            var attributes = {
                id: "SoundPlayerFreeDialog",
                name: "SoundPlayerFreeDialog"
            };
            window.swfobject.embedSWF(path, "SoundContainerFreeDialog", "195", "28", "6.0.0", "http://get.adobe.com/flashplayer/", flashvars, params, attributes);
            window.SoundPlayerFreeDialog = document.getElementById('SoundPlayerFreeDialog');
        }
    }
}

function sp_play_free_dialog(soundStream, behaviorId, hasProgressBar) {
    if (IsUseHtml5MediaPlayerMode()) {
        sp_play_html5(soundStream, behaviorId, hasProgressBar);
    } else {
        sp_play_free_dialog_flash(soundStream, behaviorId);
    }
}

function sp_play_free_dialog_flash(soundStream, behaviorId) {
    try {
        var sp_obj = window.document.getElementById('SoundPlayerFreeDialog');
        if (sp_obj != null) {
            if (sp_obj.PlaySound == null) {
                var sp_i = 0;
                while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayerFreeDialog")) {
                    sp_i++;
                }
                if (sp_obj.childNodes[sp_i].name == "SoundPlayerFreeDialog") sp_obj = sp_obj.childNodes[sp_i];
            }
            sp_obj.PlaySound(soundStream, behaviorId);
        }
    }
    catch (ex) { }
}

function sp_pause_free_dialog() {
    if (IsUseHtml5MediaPlayerMode()) {
        sp_pause_html5();
    } else {
        sp_pause_free_dialog_flash();
    }
}

function sp_pause_free_dialog_flash() {
    try {
        var sp_obj = window.document.getElementById('SoundPlayerFreeDialog');
        if (sp_obj != null) {
            if (sp_obj.PauseSound == null) {
                var sp_i = 0;
                while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayerFreeDialog")) {
                    sp_i++;
                }
                if (sp_obj.childNodes[sp_i].name == "SoundPlayerFreeDialog") sp_obj = sp_obj.childNodes[sp_i];
            }
            sp_obj.PauseSound();
        }
    }
    catch (ex) { }
}

function sp_stop_free_dialog() {
    if (IsUseHtml5MediaPlayerMode()) {
        sp_stop_html5(); //Stop the sound by html5
    }
    else {
        sp_stop_free_dialog_flash(); //Stop the sound by flash
    }
}

function sp_stop_free_dialog_flash() {
    try {
        var sp_obj = window.document.getElementById('SoundPlayerFreeDialog');
        if (sp_obj != null) {
            if (sp_obj.StopSound == null) {
                var sp_i = 0;
                while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayerFreeDialog")) {
                    sp_i++;
                }
                if (sp_obj.childNodes[sp_i].name == "SoundPlayerFreeDialog") sp_obj = sp_obj.childNodes[sp_i];
            }
            sp_obj.StopSound();
        }
    }
    catch (ex) { }
}

function sp_release() {
    if (IsUseHtml5MediaPlayerMode())
        return;

    try {
        var sp_obj = window.document.getElementById('SoundPlayer');
        if (sp_obj != null) {
            sp_obj.PlaySound = null;
            sp_obj.StopSound = null;
            sp_obj.PauseSound = null;
            sp_obj.start = null;
            sp_obj.startError = null;
        }
    }
    catch (ex) { }
    try {
        var sp_obj = window.document.getElementById('SoundPlayerFreeDialog');
        if (sp_obj != null) {
            sp_obj.PlaySound = null;
            sp_obj.StopSound = null;
            sp_obj.PauseSound = null;
        }
    }
    catch (ex) { }
}

// play the reco beep
function bp_start(reco) {
    //try {
    //    if (reco == null)
    //        reco = 'SpeechRecognitionBehavior1';
    //    var sp_obj = window.document.getElementById('SoundPlayer');
    //    if (sp_obj != null) {
    //        if (sp_obj.start == null) {
    //            var sp_i = 0;
    //            while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayer")) {
    //                sp_i++;
    //            }
    //            if (sp_obj.childNodes[sp_i].name == "SoundPlayer") sp_obj = sp_obj.childNodes[sp_i];
    //        }
    //        sp_obj.start(reco);
    //    }
    //    return true;
    //}
    //catch (ex) { return false; }
    this.bp_finished(reco);
}

// called when the beep is finished
function bp_finished(reco) {
    try {
        bp_obj = $find(reco);
        if (bp_obj != null) bp_obj.launchRecognition();
    }
    catch (ex) { }
}

// play the reco error beep
function bp_startError(reco) {
    try {
        if (reco == null)
            reco = 'SpeechRecognitionBehavior1';
        var sp_obj = window.document.getElementById('SoundPlayer');
        if (sp_obj != null) {
            if (sp_obj.startError == null) {
                var sp_i = 0;
                while ((sp_i < sp_obj.childNodes.length) && (sp_obj.childNodes[sp_i].name != "SoundPlayer")) {
                    sp_i++;
                }
                if (sp_obj.childNodes[sp_i].name == "SoundPlayer") sp_obj = sp_obj.childNodes[sp_i];
            }
            sp_obj.startError(reco);
        }
        return true;
    }
    catch (ex) { return false; }
}

// called when the error beep is finished
function bp_finishedError(reco) {
    try {
    }
    catch (ex) { }
}

//html5 sound player start
var playAudioTimeOut = null;
function sp_play_html5(soundUrl, behaviorId, hasProgressBar) {
    if ($("#tmm_audio_html5").attr("tmm_state") == "2") {
        var audioPlayer = document.getElementById("tmm_audio_html5");
        var currentUrl = audioPlayer.currentSrc;
        if (currentUrl != "" && soundUrl.indexOf(currentUrl) > -1) {
            $("#tmm_audio_html5").attr("tmm_state", "1");
            audioPlayer.play();
            return;
        }
    }

    if (playAudioTimeOut) {
        clearTimeout(playAudioTimeOut);
    }

    var tempSoundUrl = soundUrl;
    var tempBehaviorId = behaviorId;
    if (isWindowsPhone()) {
        var audioPlayer = document.getElementById("tmm_audio_html5");
        if (audioPlayer != null && !audioPlayer.paused) {
            $("#tmm_audio_html5").attr("tmm_behaviorId", "");
            audioPlayer.currentTime = audioPlayer.duration;
            waitLastAudioEnded();
        } else {
            sp_play_audio(soundUrl, behaviorId, hasProgressBar);
        }
    } else {
        sp_play_audio(soundUrl, behaviorId, hasProgressBar);
    }

    //wait last audio end, for windows phone
    function waitLastAudioEnded() {
        if (playAudioTimeOut) {
            clearTimeout(playAudioTimeOut);
        }

        var audioPlayer = document.getElementById("tmm_audio_html5");
        if (audioPlayer.paused && audioPlayer.ended) {
            sp_play_audio(tempSoundUrl, tempBehaviorId, hasProgressBar);
        } else {
            playAudioTimeOut = setTimeout(waitLastAudioEnded, 100);
        }
    }
}

//play audio
function sp_play_audio(soundUrl, behaviorId, hasProgressBar) {
    try {
        if (soundUrl == undefined || soundUrl == null || soundUrl == "")
            return false;

        var isFirstPlay = false;
        if ($("#tmm_audio_html5").length == 0) {
            isFirstPlay = true;
            var audioHtml = "<audio id='tmm_audio_html5'></audio>";
            $("body").append($(audioHtml)); //add audio element to current page
        }

        var tempAudioPlayer = $("#tmm_audio_html5");
        if (behaviorId == undefined || behaviorId == null || behaviorId == "") {
            tempAudioPlayer.attr("tmm_behaviorId", "");
        }
        else {
            tempAudioPlayer.attr("tmm_behaviorId", behaviorId);
        }

        if (hasProgressBar) {
            tempAudioPlayer.attr("tmm_progress_bar", "1");
        } else {
            tempAudioPlayer.attr("tmm_progress_bar", "0");
        }

        tempAudioPlayer.attr("tmm_state", "1"); //0 stop, 1 play, 2 pause
        var audioPlayer = document.getElementById("tmm_audio_html5");
        if (isFirstPlay) {
            //bing event
            audioPlayer.addEventListener("playing", function () {
                var audioBehaviorId = tempAudioPlayer.attr("tmm_behaviorId");
                if (audioBehaviorId != "") {
                    sp_sound_started(audioBehaviorId);
                }
            });
            audioPlayer.addEventListener("ended", function () {
                var audioBehaviorId = tempAudioPlayer.attr("tmm_behaviorId");
                if (audioBehaviorId != "") {
                    sp_sound_finished(audioBehaviorId);
                }
            });

            audioPlayer.addEventListener("play", function () {
                if (tempAudioPlayer.attr("tmm_state") == "1") {
                    var audioBehaviorId = tempAudioPlayer.attr("tmm_behaviorId");
                    if (audioBehaviorId != "") {
                        sp_ready(audioBehaviorId);
                    }
                } else {
                    audioPlayer.pause(); //pause the audio
                }
            });

            if ($("#soundProgressBar").length > 0) {
                audioPlayer.addEventListener("timeupdate", function() {
                    if (tempAudioPlayer.attr("tmm_progress_bar") == "0") {
                        return;
                    }

                    var soundDuration = audioPlayer.duration;
                    if (isFinite(soundDuration) && soundDuration > 0) {
                        var currentTime = audioPlayer.currentTime;
                        $("#soundDuration").text(sp_format_time(currentTime) + "/" + sp_format_time(soundDuration));

                        var progress = Math.floor(currentTime) / Math.floor(soundDuration);
                        if (soundDuration < 1) {
                            if (currentTime < soundDuration) {
                                progress = 0;
                            } else {
                                progress = 1;
                            }
                        }

                        var progressBarWidth = Math.floor(progress * $("#soundLoadingBar").width());
                        $("#soundProgressBar").width(progressBarWidth);
                    }
                });
            }

            if (isAndroid()) {
                audioPlayer.addEventListener("loadstart", function () {
                    if (audioPlayer.paused && audioPlayer.networkState == 1 && $("#tmm_audio_html5").attr("tmm_state") == "1") {
                        audioPlayer.play(); //play the audio
                    }
                });
                audioPlayer.addEventListener("canplaythrough", function () {
                    if (audioPlayer.paused && $("#tmm_audio_html5").attr("tmm_state") == "1") {
                        audioPlayer.play(); //play the audio
                    }
                });
            }
        }

        //remove all current sound from audio
        for (var i = audioPlayer.childNodes.length - 1; i >= 0; i--) {
            audioPlayer.removeChild(audioPlayer.childNodes[i]);
        }

        var soundUrlArray = soundUrl.split("|"); //get all sound urls
        //add all sound to audio
        for (var i = 0; i < soundUrlArray.length; i++) {
            var sourceItem = document.createElement("source");
            sourceItem.src = soundUrlArray[i];
            sourceItem.type = getAudioType(soundUrlArray[i]);
            audioPlayer.appendChild(sourceItem);
        }

        audioPlayer.currentSrc = "";
        audioPlayer.load(); //reload audio sound

        if (!isAndroid()) {
            audioPlayer.play(); //play the audio
        }

        return true;
    }
    catch (ex) { return false; }
}

function sp_format_time(time) {
    var m = Math.floor(time / 60);
    var s = Math.floor(time % 60);

    return m.lead0(2) + ":" + s.lead0(2);
}

function sp_pause_html5() {
    var audioPlayer = document.getElementById("tmm_audio_html5");
    if (audioPlayer != null) {
        $("#tmm_audio_html5").attr("tmm_state", "2");
        audioPlayer.pause(); //pause the audio
    }
}

//stop audio
function sp_stop_html5() {
    var audioPlayer = document.getElementById("tmm_audio_html5");
    if (audioPlayer != null) {
        $("#tmm_audio_html5").attr("tmm_state", "0");
        audioPlayer.currentTime = 0;
        audioPlayer.pause(); //pause the audio
    }
}

//get audio type
function getAudioType(audioUrl) {
    var audioUrlLower = audioUrl.toLowerCase();
    if (audioUrlLower.indexOf("extension=ogg") > 0) {
        return "audio/ogg";
    }
    else {
        return "audio/mpeg";
    }
}

//html5 sound player end