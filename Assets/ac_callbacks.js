// get words to recognize
function acSpeechRecognitionGetWords(reco)
{
    var rs_obj = $find(reco);
    if (rs_obj != null)
    {
        if (rs_obj._recoState == 0)
            return "_stop_";
        var rs_res = rs_obj.get_Vocabulary();
        if (rs_res == null)
            rs_res = "";
        return rs_res;
    }
    else return null;
}

// get words to recognize for SETS
function acSpeechRecognitionGetSets(reco)
{
    var rs_obj = $find(reco);
    if (rs_obj != null)
    {
        var rs_res = rs_obj.get_VocabularyForSets();
        if (rs_res == null)
            rs_res = "";
        return rs_res;
    }
    else return "";
}

// indicates the recognition started
function acStartRecording(reco)
{
    var rs_obj = $find(reco);
    if (rs_obj != null) rs_obj.informStarted();
}

// indicates speech was detected
function acRecordingSpeech(reco)
{
    var rs_obj = $find(reco);
    if (rs_obj != null) rs_obj.hideFlashRecoServer();
}

// indicates recognition is processing
function acRecoProcessing(reco)
{
    var rs_obj = $find(reco);
    if (rs_obj != null)
    {
        if (rs_obj._recoState != 0)
            rs_obj.makeEvaluating();
    }
}

// set recognition results
function acRecordingSuccess(reco, index, score, setsIndex, curves)
{
    var rs_obj = $find(reco);
    if(rs_obj != null)
    {
        rs_obj.setResults(index, score, setsIndex, curves);
    }
}

// indicates an error occured
function acRecordingError(reco)
{
    var rs_obj = $find(reco);
    if(rs_obj != null)
    {
        rs_obj.makeError();
    }
}

// indicates the sound finished playing
function acSoundStopped(reco)
{
    var rs_obj = $find(reco);
    if(rs_obj != null)
    {
        rs_obj.notifySoundFinished();
    }
}

// display the sound level
function acDefineSoundLevel(reco, level)
{
    var rs_obj = $find(reco);
    if(rs_obj != null) rs_obj.setSoundLevel(acTranslateSoundEnergyForFlash(level));
}

// display the sound level
function acAddHistoryItem(reco, streamName)
{
    var rs_obj = $find(reco);
    if(rs_obj != null) rs_obj.addStreamName(streamName);
}

// conversion of levels between speech recognition and graphical component
function acTranslateSoundEnergyForFlash(level)
{
//    if (level >= 90) return 8;
//    if (level >= 75) return 7;
//    if (level >= 62) return 6;
//    if (level >= 45) return 5;
//    if (level >= 30) return 4;
//    if (level >= 20) return 3;
//    if (level >= 6) return 2;
//    if (level >= 0) return 1;
//    else return 0;
    return level;
}

// function called to retry opening a connection with the speech recognition server
function acRetryConnectingToRecoServer()
{
    if (window.document.acSpeechRecognitionRecoServer == null)
        window.document.acSpeechRecognitionRecoServer = document.getElementById('acSpeechRecognitionRecoServer');
    if (window.document.acSpeechRecognitionRecoServer != null)
    {
        window.document.acSpeechRecognitionRecoServer.retryConnectingToRecoServer();
    }
}

// function called to get the number of consecutive timeouts
function acGetNbTimeOut() {
    var cookieName = "tmmrecotimeout=";
    var cookieNumber = 0;
    try {
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(cookieName) == 0)
                cookieNumber = parseInt(c.substring(cookieName.length, c.length));
        }
    }
    catch (e) { }
    return cookieNumber;
}

// function called to increment the number of consecutive timeouts
function acTimeOut() {
    var cookieNumber = acGetNbTimeOut();
    var cookieName = "tmmrecotimeout=";
    cookieNumber++;
    try {
        document.cookie = cookieName + cookieNumber + ';path=/';
    }
    catch (e) { }
}

// function called to reset the number of consecutive timeouts
function acResetTimeOut() {
    try {
        document.cookie = 'tmmrecotimeout=0;path=/';
    }
    catch (e) { }
}

// function called to get the number of times the timeout box was showed
function acGetNbTimeOutBox() {
    var cookieName = "tmmrecotimeoutbox=";
    var cookieNumber = 0;
    try {
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(cookieName) == 0)
                cookieNumber = parseInt(c.substring(cookieName.length, c.length));
        }
    }
    catch (e) { }
    return cookieNumber;
}

// function called to increment the number of times the timeout box was showed
function acTimeOutBox() {
    var cookieNumber = acGetNbTimeOutBox();
    var cookieName = "tmmrecotimeoutbox=";
    cookieNumber++;
    try {
        document.cookie = cookieName + cookieNumber + ';path=/';
    }
    catch (e) { }
}

//code javascript for micro settings
function acMicroRecordAndPlayFlashObjectLoaded () {
//    console.log("RecoServerCtrl.js, method acMicroRecordAndPlayFlashObjectLoaded");
}

function acRecoServerFlashObjectLoaded() {
//    console.log("RecoServerCtrl.js, method acRecoServerFlashObjectLoaded");
}

function acGetMicroSettingsData() {
//    console.log("RecoServerCtrl.js, method acGetMicroSettingsData: Begin");
    //var data = window.acMicroRecordAndPlay.msGetMicroSettingsData();
    var data = Android.msGetMicroSettingsData();
//    console.log("RecoServerCtrl.js, method acGetMicroSettingsData: micro settings data: " +data);
    Android.setMicroSettingsData(data);
    //window.acSpeechRecognitionRecoServer.setMicroSettingsData(data);
//    console.log("RecoServerCtrl.js, method acGetMicroSettingsData: End");
}