//
//  main.m
//  flash-webview
//
//  Created by git on 03/13/2018.
//  Copyright (c) 2018 git. All rights reserved.
//

@import UIKit;
#import "FWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FWAppDelegate class]));
    }
}
