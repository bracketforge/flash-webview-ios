//
//  FWAppDelegate.h
//  flash-webview
//
//  Created by git on 03/13/2018.
//  Copyright (c) 2018 git. All rights reserved.
//

@import UIKit;

@interface FWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
