//
//  FWViewController.m
//  flash-webview
//
//  Created by git on 03/13/2018.
//  Copyright (c) 2018 git. All rights reserved.
//

#import "FWViewController.h"
@import WebKit;
@import flash_webview;
@import UIKit;


@interface FWViewController ()

@end

@implementation FWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    WKWebView * webView = [[WKWebView alloc] init];
    [self.view addSubview:webView];

    [[WebViewHandler sharedInstance] setupWebViewContainer:webView url:@"https://cnpro1.rosettastone.cn/TellMeMore/SystemManagement/FreeMode/WorkshopSpeaking.aspx"];


    UIButton * button = [[UIButton alloc] init];
    button.frame = CGRectMake(150, 0, 100, 40);
    [button setTitle:@"Restart connection" forState:UIControlStateNormal];
    [button sizeToFit];

    [self.view addSubview:button];
    [self.view bringSubviewToFront:button];

    [button addTarget:self action: @selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)buttonTapped:(NSObject *)sender {

    [[WebViewHandler sharedInstance] restart];
}

@end
